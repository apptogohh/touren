# README

Touren is a simple app to create and take part in interactive tours. Tours consist of locations that can have GPS coordinates or not. Locations can have different content items attached to them, as well as one action. You can enforce whether these actions have to be done before advancing to the next location. You can also enforce whether a user has to be close to a location in order to access its action.

Actions are:

- instruction (a simple ToDo)
- question (free-text)
- multiple choice

Other content items are:

- text
- image
- link
- panorama image (360°)

The app is browser based, offline first and mobile ready.
All content except panorama images is cached locally.

It was initially developed by [Universität Hamburg](https://www.uni-hamburg.de/) supported by [Hamburg Open Online University](https://www.hoou.de/).

## Key Technologies

- https://rubyonrails.org/
- https://reactjs.org/
- https://www.postgresql.org/
- https://github.com/material-components/material-components-web
- https://pannellum.org/

## Development and Testing

### Setup DB

User `touren_development` and password `touren_development` are hardcoded in `database.yml`.
We tested against Postgres 11.

### Setup Webpacker

Set `export SASS_PATH=./node_modules` in all terminals (e.g. put it in your `.profile`)

### Start Dev Server

Start dev server: `rails s`
Start autoreloading: `bin/webpack-dev-server`

### Specs

Watch js specs: `yarn test-watch`
Watch rb specs: `bundle exec guard`

Run features with visible browser: `SHOW=t rspec spec/features/basics_spec.rb`

Have fun!

### Lint

`rubocop -a`

### Shibboleth

Config SAML endpoint via `/config/initializers/devise.rb`. A good tutorial can be found here: https://medium.com/@johnrcallahan/shibboleth-for-beginners-part-1-f8fb59b87fa2

### Create sample data

```
# Create tour
rake gen:c[tour]
rake gen:create[tour]

# Create tour using with_locations trait
rake gen:c[tour,with_locations]

# Create 10 tours
rake gen:create_list[tour,10]
rake gen:cl[tour,10]

# Create 10 tours using with_locations trait
rake gen:cl[tour,10,with_locations]

# Truncate db tables
rake gen:d[Model]
rake gen:destroy[Model]
rake gen:destroy[Model1,Model2,Model3]
rake gen:destroy[model1,model2,model3]

# Truncate all db tables
rake db:t
rake db:truncate
```
