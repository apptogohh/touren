# Touren App

## Big Goals

It is our goal to present in the middle of March an app that can be used to author and take part in tours of GPS locations.

The order of a tour can be fixed and the proximity to locations required.

Each location may include multiple content items (images, texts and 360 degree images), as well as links to videos.

There can also be one confirmable action (press OK), one free text question or one multiple choice question per location.

It should be possible to upload a collection of all answers concerning a tour along with a string identifier (pseudonym) to the server, so an author can view/grade them.

It should be possible to hide tours behind a password.

Authors should log in via a shibboleth server.

## Small Stuff

The first image of a tour/location should be thumbnailed and used in lists.

We should anticipate/prepare for later translations.
