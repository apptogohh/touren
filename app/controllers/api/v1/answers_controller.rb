class API::V1::AnswersController < API::V1::ApiController
  before_action :find_tour
  before_action :check_tour_password

  def create
    @answer = @tour.answers.new(answer_params)

    if @answer.save
      render json: { status: :success, created_at_string: I18n.l(@answer.created_at, format: :long) }.to_json
    else
      render json: { status: :error, errors: @answer.errors }, status: 500
    end
  end

  private

  def find_tour
    @tour = Tour.find(params[:tour_id])
  end

  def answer_params
    params.require(:answer).permit(:alias, :json)
  end
end
