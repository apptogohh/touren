class API::V1::ApiController < ApplicationController
  respond_to :json
  skip_before_action :verify_authenticity_token

  protected

  def check_tour_password
    if @tour.password_hash.present? && @tour.password_hash != request.headers['X-Password'] # rubocop:disable Style/GuardClause, Metrics/LineLength
      render json: { status: :wrong_password }.to_json, status: 400
    end
  end
end
