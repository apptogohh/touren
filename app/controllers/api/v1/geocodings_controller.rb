class API::V1::GeocodingsController < API::V1::ApiController
  def create
    render json: {
      geocodings: Geocoding.search(params[:q]).map(&:as_api_json)
    }
  end
end
