class API::V1::LocationsController < API::V1::ApiController
  before_action :find_tour
  before_action :check_tour_password

  def index
    render json: {
      tourId: @tour.id,
      locations: @tour.locations.map(&:as_api_json)
    }
  end

  private

  def find_tour
    @tour = Tour.find(params[:tour_id])
  end
end
