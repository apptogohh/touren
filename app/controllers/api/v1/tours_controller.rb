class API::V1::ToursController < API::V1::ApiController
  before_action :find_tour, only: :show
  before_action :check_tour_password, only: :show

  def index
    render json: {
      tours: Tour.where(password_hash: [nil, '']).map(&:as_api_json),
      toursCatalog: Tour.all.map(&:as_api_json_catalog)
    }
  end

  def show
    render json: { tour: @tour.as_api_json }
  end

  def password # rubocop:disable Metrics/AbcSize
    tour = Tour.where(password_hash: request.headers['X-Password']).first if request.headers['X-Password'].present?

    if tour.present?
      render json: { status: :success, tourId: tour.id }.to_json
    else
      render json: { status: :wrong_password }.to_json, status: 400
    end
  end

  private

  def find_tour
    @tour = Tour.find(params[:id])
  end
end
