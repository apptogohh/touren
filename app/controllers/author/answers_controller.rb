class Author::AnswersController < Author::AuthorController
  def index
    @answers = current_author.answers.order(created_at: :desc)
  end

  def destroy
    answer = current_author.answers.find(params[:id])
    tour_id = answer.tour_id
    answer.destroy!
    redirect_to "/author/answers?tour_id=#{tour_id}"
  end

  def show
    answer = current_author.answers.find(params[:id])
    tour_id = answer.tour_id
    redirect_to "/author/answers?tour_id=#{tour_id}"
  end
end
