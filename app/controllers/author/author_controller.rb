class Author::AuthorController < ApplicationController
  begin
    before_action :authenticate_author!
  rescue StandardError
    redirect_to root_path
  end
  layout 'author'
end
