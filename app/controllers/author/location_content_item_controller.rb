class Author::LocationContentItemController < Author::AuthorController
  before_action :find_location
  before_action :find_content_item, only: %i[show destroy]

  def new
    @content_item = build_content_item
  end

  def create
    @content_item = build_content_item(content_item_params)
    if @content_item.save
      redirect_to author_location_url(@content_item.location_id)
    else
      render 'new'
    end
  end

  def show; end

  def destroy
    @content_item.destroy!
    redirect_to author_location_url(@content_item.location_id)
  end

  private

  def build_content_item(content_item_params = {})
    @location.send(content_item_class.to_s.tableize).new(content_item_params)
  end

  def find_location
    @location = current_author.locations.find(params[:location_id])
  end

  def find_content_item
    @content_item = @location.send(content_item_class.to_s.tableize).find(params[:id])
  end
end
