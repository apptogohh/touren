class Author::LocationContentItemsController < Author::AuthorController
  before_action :find_location

  def new
    redirect_to redirect_path
  end

  def move_higher
    content_item = @location.location_content_items.find(params[:id])
    content_item.move_higher
    redirect_to author_location_path(@location.id)
  end

  private

  def redirect_path # rubocop:disable Metrics/AbcSize, Metrics/MethodLength, Metrics/CyclomaticComplexity
    case location_content_item_params[:type]
    when LocationImage.to_s
      new_author_location_location_image_path(params[:location_id])
    when LocationPanorama.to_s
      new_author_location_location_panorama_path(params[:location_id])
    when LocationMultichoice.to_s
      new_author_location_location_multichoice_path(params[:location_id])
    when LocationText.to_s
      new_author_location_location_text_path(params[:location_id])
    when LocationQuestion.to_s
      new_author_location_location_question_path(params[:location_id])
    when LocationInstruction.to_s
      new_author_location_location_instruction_path(params[:location_id])
    when LocationLink.to_s
      new_author_location_location_link_path(params[:location_id])
    else
      flash[:notice] = I18n.t('choose_content_item')
      author_location_path(params[:location_id])
    end
  end

  def find_location
    @location = Location.find(params[:location_id])
  end

  def location_content_item_params
    params.require(:location_content_item).permit(:type)
  end
end
