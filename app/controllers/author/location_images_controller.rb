class Author::LocationImagesController < Author::LocationContentItemController
  private

  def content_item_class
    LocationImage
  end

  def content_item_params
    params.require(:location_image).permit(:name, :description, :location_image_file)
  end
end
