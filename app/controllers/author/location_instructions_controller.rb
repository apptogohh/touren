class Author::LocationInstructionsController < Author::LocationContentItemController
  before_action :name_to_content, only: %i[create]

  private

  def content_item_class
    LocationInstruction
  end

  def content_item_params
    params.require(:location_instruction).permit(:name, :content)
  end

  def name_to_content
    params[:location_instruction][:content] = params[:location_instruction][:name]
  end
end
