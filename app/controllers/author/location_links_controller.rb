class Author::LocationLinksController < Author::LocationContentItemController
  private

  def content_item_class
    LocationLink
  end

  def content_item_params
    params.require(:location_link).permit(:name, :content)
  end
end
