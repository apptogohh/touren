class Author::LocationMultichoicesController < Author::LocationContentItemController
  before_action :choices_to_content, only: %i[create]

  private

  def choices_to_content
    choices = params[:location_multichoice][:choices]
    correct_choices = params[:location_multichoice][:correct_choices]
    return unless choices && correct_choices

    params[:location_multichoice][:content] = params_to_json(
      choices,
      correct_choices,
      params[:location_multichoice][:name]
    )
  end

  def params_to_json(choices, correct_choices, question)
    {
      question: question,
      choices: choices.zip(correct_choices).map do |choice, correct|
        { choice: choice, correct: correct == 'true' }
      end
    }.to_json
  end

  def content_item_class
    LocationMultichoice
  end

  def content_item_params
    params.require(:location_multichoice).permit(:name, :content)
  end
end
