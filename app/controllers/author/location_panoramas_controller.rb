class Author::LocationPanoramasController < Author::LocationContentItemController
  private

  def content_item_class
    LocationPanorama
  end

  def content_item_params
    params.require(:location_panorama).permit(:name, :description, :location_image_file)
  end
end
