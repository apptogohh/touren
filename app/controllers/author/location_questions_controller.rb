class Author::LocationQuestionsController < Author::LocationContentItemController
  before_action :name_to_content, only: %i[create]

  private

  def content_item_class
    LocationQuestion
  end

  def content_item_params
    params.require(:location_question).permit(:name, :content)
  end

  def name_to_content
    params[:location_question][:content] = params[:location_question][:name]
  end
end
