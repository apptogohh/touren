class Author::LocationTextsController < Author::LocationContentItemController
  private

  def content_item_class
    LocationText
  end

  def content_item_params
    params.require(:location_text).permit(:name, :content)
  end
end
