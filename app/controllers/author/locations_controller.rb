class Author::LocationsController < Author::AuthorController
  before_action :find_location, only: %i[move_higher show destroy update]

  def new
    @location = Location.new(tour_id: params[:tour_id])
  end

  def move_higher
    @location.move_higher
    redirect_to author_tour_path(@location.tour_id)
  end

  def create
    current_author.tours.find(location_params[:tour_id])
    @location = Location.new(location_params)

    if @location.save
      redirect_to author_location_url(@location)
    else
      render 'new'
    end
  end

  def show
    @location_content_item = LocationContentItem.new
  end

  def destroy
    @location.destroy!
    redirect_to author_tour_url(@location.tour_id)
  end

  def update
    @location.update_attributes!(location_update_params)
    flash[:success] = I18n.t('saved')
    redirect_to author_location_url(@location)
  end

  private

  def find_location
    @location = current_author.locations.find(params[:id])
  end

  def clean_coord(coord)
    coord.tr(',', '.').to_f
  end

  def location_params
    cleaned = params.require(:location).permit(:name, :lat, :lng, :tour_id)
    cleaned[:lat] = clean_coord(cleaned[:lat])
    cleaned[:lng] = clean_coord(cleaned[:lng])
    cleaned
  end

  def location_update_params
    params.require(:location).permit(:name, :lat, :lng)
  end
end
