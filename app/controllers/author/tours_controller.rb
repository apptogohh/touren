class Author::ToursController < Author::AuthorController
  before_action :find_tour, only: %i[show destroy update]

  def index
    @tours = current_author.tours
  end

  def new
    @tour = Tour.new
  end

  def create
    @tour = Tour.new(tour_params)
    @tour.author = current_author
    if @tour.save
      redirect_to author_tour_url(@tour)
    else
      render 'new'
    end
  end

  def show; end

  def destroy
    @tour.destroy!
    redirect_to author_tours_url
  end

  def update
    @tour.update_attributes!(tour_update_params)
    flash[:success] = I18n.t('saved')
    redirect_to author_tour_url(@tour)
  end

  private

  def find_tour
    @tour = current_author.tours.find(params[:id])
  end

  def tour_params
    params.require(:tour).permit(:name, :tour_image_file, :enforce_sequence, :enforce_proximity, :password)
  end

  def tour_update_params
    params.require(:tour).permit(:enforce_sequence, :enforce_proximity, :password, :tour_image_file)
  end
end
