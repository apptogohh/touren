# https://medium.com/@fkadev/how-i-implemented-css-modules-to-ruby-on-rails-easily-abb324ce22d
# https://www.visuality.pl/posts/css-modules-in-rails

module CssHelper
  def css_class(module_name, selector = nil)
    selector = module_name if selector.nil?
    out = "#{module_name}__#{selector}"
    "touren__#{out}__#{css_encode(out)}"
  end

  def css_bem(block, element = '', modifier = '')
    if element.blank?
      out = "#{block}__#{block}"
    else
      out = "#{block}__#{block}__#{element}"
      out += "--#{modifier}" if modifier.present?
    end
    "touren__#{out}__#{css_encode(out)}"
  end

  def css_encode(name)
    Base64.encode64(name).gsub(/\W/, '')
  end

  class Styler
    def initialize(module_name, helper)
      @module_name = module_name
      @helper = helper
    end

    def css_class(selector = nil)
      @helper.css_class(@module_name, selector)
    end

    def css_bem(element = '', modifier = '')
      @helper.css_bem(@module_name, element, modifier)
    end
  end

  def css_component(module_name, &block)
    block.call(Styler.new(module_name.to_s, self)) # rubocop:disable Performance/RedundantBlockCall
  end
end
