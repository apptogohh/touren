module FlashMessagesHelper
  def flash_messages # rubocop:disable Metrics/MethodLength
    flash_messages = []
    flash.each do |type, message|
      t = {
        success: 'information',
        error: 'error',
        alert: 'alert',
        notice: 'warning'
      }[type.to_sym]
      text = "<script>
        document.addEventListener('DOMContentLoaded', function() {
          noty('#{message}', '#{t}');
        });
      </script>"
      flash_messages << text.html_safe if message
    end
    flash_messages.join("\n").html_safe
  end
end
