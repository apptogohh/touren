module LinkToDestroyHelper
  def link_to_destroy(body, path, class_name = '')
    link_to body, path, method: :delete, data: { confirm: t('confirm_sureness') }, class: class_name
  end
end
