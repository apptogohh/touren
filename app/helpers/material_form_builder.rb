# rubocop:disable Metrics/MethodLength
# rubocop:disable Metrics/AbcSize
class MaterialFormBuilder < ActionView::Helpers::FormBuilder # rubocop:disable Metrics/ClassLength
  include Rails.application.routes.url_helpers

  def m_check_box(method, options = {})
    l = if options[:label].present?
          options[:label].html_safe
        else
          resolve_label(method)
        end

    check_box_options = { class: 'mdc-checkbox__native-control' }
    check_box_options[:onclick] = options[:onclick] if options[:onclick].present?

    @template.content_tag :div, class: "mdc-form-field #{@template.css_class(:mFormField)}" do
      @template.concat(@template.content_tag(:div, class: 'mdc-checkbox') do
        @template.concat(check_box(method, **check_box_options))
        @template.concat('<div class="mdc-checkbox__background">
              <svg class="mdc-checkbox__checkmark"
                   viewBox="0 0 24 24">
                <path class="mdc-checkbox__checkmark-path"
                      fill="none"
                      d="M1.73,12.91 8.1,19.28 22.79,4.59"/>
              </svg>
              <div class="mdc-checkbox__mixedmark"></div>
            </div>'.html_safe)
      end)
      @template.concat(label(method, l))
    end
  end

  def m_text_field(method, options = {})
    container_class = if options.key?(:override_container_class)
                        options[:override_container_class].to_s
                      else
                        'mdc-text-field' + options[:container_class].to_s
                      end

    input_class = if options.key?(:override_input_class)
                    options[:override_input_class].to_s
                  else
                    'mdc-text-field__input ' + options[:input_class].to_s
                  end

    input_options = { class: input_class }

    input_options[:oninput] = options[:oninput] if options[:oninput]

    @template.content_tag :p do
      @template.concat(
        @template.content_tag(:div, class: container_class, style: options[:container_style].to_s) do
          @template.concat text_field(method, **input_options)
          _label(method, options)
        end
      )
      @template.concat @template.content_tag(:div, nil, class: 'mdc-line-ripple')
    end
  end

  def m_file_field(method, options = {})
    @template.css_component(:mFileUpload) do |c|
      @template.content_tag(:div, class: c.css_bem) do
        button_class = "#{c.css_bem(:button)} mdc-button mdc-js-button mdc-button--raised mdc-js-ripple-effect mdc-button--accent" # rubocop:disable Metrics/LineLength
        @template.concat(@template.content_tag(:div, class: button_class) do
          @template.concat @template.content_tag(:span, I18n.t('browse'))
          @template.concat file_field(method, class: c.css_bem(:fileInput), onchange: options[:onchange])
        end)
        @template.concat(
          @template.text_field_tag(
            '',
            '',
            class: c.css_bem(:status),
            placeholder: options[:placeholder],
            readonly: 'readonly'
          )
        )
      end
    end
  end

  def m_image_field(method, options = {})
    @template.capture do
      value = object.send(method)
      if value.attached?
        @template.concat(
          hidden_field(method, value: value.signed_id)
        )
      end
      @template.concat m_file_field(method, options)
      @template.concat(
        @template.content_tag(:div, class: 'mt-3') do
          if value.attached?
            @template.link_to(value, target: '_blank') do
              @template.image_tag value.variant(resize: '150x150').processed
            end
          end
        end
      )
    end
  end

  def m_text_area(method, options = {})
    container_class = if options.key?(:override_container_class)
                        options[:override_container_class].to_s
                      else
                        'mdc-text-field mdc-text-field--textarea ' + options[:container_class].to_s
                      end

    input_class = if options.key?(:override_input_class)
                    options[:override_input_class].to_s
                  else
                    'mdc-text-field__input ' + options[:input_class].to_s
                  end

    options[:rows] = 10 unless options[:rows].present?

    @template.content_tag :p do
      @template.concat(
        @template.content_tag(:div, class: container_class, style: options[:container_style].to_s) do
          # With label
          @template.concat text_area(method, class: input_class)
          _label(method, options)

          # Placeholder instead of label
          # @template.concat text_area(method, class: input_class, placeholder: method)
        end
      )
    end
  end

  def m_submit(text = I18n.t('submit'), options = {})
    html_options = options.slice(:id, :disabled)
    @template.content_tag(:p) do
      @template.concat(submit(text, class: "mdc-button mdc-button--outlined #{options[:class]}", **html_options))
      @template.concat(@template.content_tag('span', options[:notice], class: 'notice'))
    end
  end

  def m_select(method, choices, select_options = {})
    @template.content_tag(:div, class: 'mdc-select') do
      @template.concat('<i class="mdc-select__dropdown-icon"></i>'.html_safe)
      @template.concat select(method, choices, select_options, class: 'mdc-select__native-control')
      @template.concat(
        @template.content_tag(:label, resolve_label(method), class: 'mdc-floating-label')
      )
      @template.concat('<div class="mdc-line-ripple"></div>'.html_safe)
    end
  end

  protected

  def _label(method, options)
    return if options.key?(:label) && options[:label] == false

    l = if options[:label].present?
          options[:label].html_safe
        else
          resolve_label(method)
        end

    label_class = 'mdc-floating-label ' + options[:label_class].to_s

    @template.concat label(method, l, class: label_class).html_safe
  end

  def resolve_label(method)
    key = 'attributes.' + method.to_s
    # raise "No translation for #{k}" if !I18n.exists?(k)
    I18n.t(key, default: method.to_s.tr('_', ' ').capitalize)
  end
end
# rubocop:enable Metrics/MethodLength
# rubocop:enable Metrics/AbcSize
