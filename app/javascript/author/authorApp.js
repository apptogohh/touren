import { blockm } from '../utils/block';

$(function() {
  const b = blockm('authorApp');

  b.ef('toggleMenu').click(function(e) {
    e.preventDefault();
    b.ef('sidebar').toggleClass('mdc-drawer--open');
  });

  b.ef('goBack').click(function(e) {
    e.preventDefault();
    window.history.go(-1);
  });
});
