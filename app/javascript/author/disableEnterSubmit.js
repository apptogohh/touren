$(document).on('keypress', 'input', function(e) {
  const code = e.keyCode || e.which;
  if (code == 13) {
    e.preventDefault();
    return false;
  }
});
