import { blockm } from '../utils/block';

$(() => {
  const b = blockm('mFileUpload');

  b.ef('fileInput').change(function() {
    $(this)
      .closest(b.b)
      .find(b.e('status'))
      .val(this.value.substring(this.value.lastIndexOf('\\') + 1));
  });
});
