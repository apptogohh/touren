/* eslint-disable import/no-extraneous-dependencies */
import { MDCTextField } from '@material/textfield/index';
import { MDCTopAppBar } from '@material/top-app-bar/index';
import { MDCFormField } from '@material/form-field';
import { MDCCheckbox } from '@material/checkbox';
import { MDCSelect } from '@material/select';
import { MDCRipple } from '@material/ripple';

$(() => {
  const appBars = document.querySelectorAll('.mdc-top-app-bar');
  appBars.forEach(appBar => new MDCTopAppBar(appBar));

  const iconButtons = document.querySelectorAll('.mdc-icon-button');
  iconButtons.forEach(iconButton => {
    const iconButtonRipple = new MDCRipple(iconButton);
    iconButtonRipple.unbounded = true;
  });

  const textFields = document.querySelectorAll('.mdc-text-field');
  textFields.forEach(textField => new MDCTextField(textField));

  const selects = document.querySelectorAll('.mdc-select');
  selects.forEach(select => new MDCSelect(select));

  const formFields = document.querySelectorAll('.mdc-form-field');
  formFields.forEach(selected => {
    const checkbox = selected.querySelector('.mdc-checkbox');
    if (checkbox) {
      const formField = new MDCFormField(selected);
      formField.input = new MDCCheckbox(checkbox);
    }
  });
});
