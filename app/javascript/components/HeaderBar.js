/* eslint-disable react/prop-types */

// Our own component gives more flixibility than TopAppBar.
// Examples:
// https://github.com/material-components/material-components-web-catalog/blob/d4ee2cc1913badf8c759bbab91d93d79dcd44301/src/HeaderBar.js
// https://material.io/develop/web/components/top-app-bar/

import React, { Component } from 'react';

class HeaderBar extends Component {
  render() {
    return (
      <header className="mdc-top-app-bar" style={{ zIndex: 7 }}>
        {' '}
        {/* TODO: Find a way how to get rid of zIndex */}
        <div className="mdc-top-app-bar__row">
          {(this.props.title || this.props.navigationIcons) && (
            <section className="mdc-top-app-bar__section mdc-top-app-bar__section--align-start">
              {this.props.navigationIcons}
              {this.props.title && (
                <span className="mdc-top-app-bar__title">
                  {this.props.title}
                </span>
              )}
            </section>
          )}
          {this.props.actionItems && (
            <section
              className="mdc-top-app-bar__section mdc-top-app-bar__section--align-end"
              role="toolbar"
            >
              {this.props.actionItems}
            </section>
          )}
        </div>
      </header>
    );
  }
}

export default HeaderBar;
