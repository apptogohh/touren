import React from 'react';
import PropTypes from 'prop-types';
import Card, { CardPrimaryContent } from '@material/react-card';
import { Body1 } from '@material/react-typography';

const ItemsNotFound = ({ children }) => (
  <Card className="mdc-card--outlined mdc-card--padded">
    <CardPrimaryContent>
      <Body1>{children}</Body1>
    </CardPrimaryContent>
  </Card>
);

ItemsNotFound.propTypes = {
  children: PropTypes.node
};

export default ItemsNotFound;
