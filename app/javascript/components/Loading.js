import React from 'react';
import Layout from '../containers/Layout';

const Loading = () => {
  return (
    <Layout>
      <img src="/images/preloader.gif" className="mt-3" />
    </Layout>
  );
};

export default Loading;
