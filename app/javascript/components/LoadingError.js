import React from 'react';
import MaterialIcon from '@material/react-material-icon';
import Button from '@material/react-button';
import Card, { CardPrimaryContent } from '@material/react-card';
import { Body1, Body2 } from '@material/react-typography';
import Layout from '../containers/Layout';

const LoadingError = () => {
  return (
    <Layout>
      <Card className="mdc-card--outlined mdc-card--padded">
        <CardPrimaryContent>
          <Body1>{I18n.t('loading_failure')}</Body1>
        </CardPrimaryContent>
        <Body2>
          <Button
            href="#"
            icon={<MaterialIcon icon="refresh" />}
            onClick={e => {
              e.preventDefault();
              location.reload();
            }}
          >
            {I18n.t('try_one_more_time')}
          </Button>
          <Button
            href="#"
            icon={<MaterialIcon icon="keyboard_arrow_left" />}
            onClick={e => {
              e.preventDefault();
              window.history.go(-1);
            }}
          >
            {I18n.t('go_to_previous_page')}
          </Button>
        </Body2>
      </Card>
    </Layout>
  );
};

export default LoadingError;
