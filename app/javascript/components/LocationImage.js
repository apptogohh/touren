import React from 'react';
import PropTypes from 'prop-types';
import Card, { CardPrimaryContent /*, CardMedia*/ } from '@material/react-card';

const ContentItemImage = ({ item }) => (
  <Card className="mdc-card--outlined mdc-card--padded">
    <CardPrimaryContent>
      {/* <CardMedia wide imageUrl={item.imageURL} alt={item.description} /> */}
      <img
        src={item.imageURL}
        alt={item.description}
        style={{ width: '100%' }}
      />
    </CardPrimaryContent>
  </Card>
);

ContentItemImage.propTypes = {
  item: PropTypes.object
};

export default ContentItemImage;
