import React from 'react';
import PropTypes from 'prop-types';
import Card, { CardActions, CardActionButtons } from '@material/react-card';
import Button from '@material/react-button';
import MaterialIcon from '@material/react-material-icon';

const ContentItemText = ({ item }) => (
  <Card className="mdc-card--outlined mdc-card--padded">
    <CardActions>
      <CardActionButtons>
        <Button
          href={item.content}
          icon={<MaterialIcon icon="link" />}
          target="_blank"
        >
          {item.name}
        </Button>
      </CardActionButtons>
    </CardActions>
  </Card>
);

ContentItemText.propTypes = {
  item: PropTypes.object
};

export default ContentItemText;
