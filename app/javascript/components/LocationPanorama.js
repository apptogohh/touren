import React from 'react';
import MaterialIcon from '@material/react-material-icon';
import PropTypes from 'prop-types';
import Card, { CardActions, CardActionButtons } from '@material/react-card';
import Button from '@material/react-button';

const LocationPanorama = ({ item }) => (
  <Card className="mdc-card--outlined mdc-card--padded">
    <CardActions>
      <CardActionButtons>
        <Button
          href={`/panorama.html?panoramaURL=${item.imageURL}`}
          icon={<MaterialIcon icon="360" />}
          target="_blank"
        >
          {item.name}
        </Button>
      </CardActionButtons>
    </CardActions>
  </Card>
);

LocationPanorama.propTypes = {
  item: PropTypes.object
};

export default LocationPanorama;
