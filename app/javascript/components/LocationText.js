import React from 'react';
import PropTypes from 'prop-types';
import Card, { CardPrimaryContent } from '@material/react-card';
import { Body1 } from '@material/react-typography';

const ContentItemText = ({ item }) => (
  <Card className="mdc-card--outlined mdc-card--padded">
    <CardPrimaryContent>
      <Body1>{item.content}</Body1>
    </CardPrimaryContent>
  </Card>
);

ContentItemText.propTypes = {
  item: PropTypes.object
};

export default ContentItemText;
