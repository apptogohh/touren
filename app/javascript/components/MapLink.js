import React from 'react';
import Button from '@material/react-button';
import MaterialIcon from '@material/react-material-icon';

// eslint-disable-next-line react/prop-types
const MapLink = ({ location }) => {
  let protocol;
  const lat = location.lat;
  const lng = location.lng;
  if (
    navigator.platform.indexOf('iPhone') !== -1 ||
    navigator.platform.indexOf('iPad') !== -1 ||
    navigator.platform.indexOf('iPod') !== -1
  ) {
    protocol = 'maps';
  } else {
    protocol = 'https';
  }

  return (
    <Button
      icon={<MaterialIcon icon="navigation" />}
      onClick={() => {
        var confirmation = confirm(I18n.t('privacy_question'));
        if (confirmation == true) {
          window.location.href = `${protocol}://www.google.com/maps/search/?api=1&query=${lat},${lng}`;
        } else {
          return false;
        }
      }}
    >
      {I18n.t('navigation_link')}
    </Button>
  );
};

export default MapLink;
