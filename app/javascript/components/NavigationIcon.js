/* eslint-disable react/prop-types */
import React from 'react';

const NavigationIcon = props => {
  return (
    <a
      href="#"
      className="material-icons mdc-top-app-bar__navigation-icon"
      onClick={props.onClick}
    >
      {props.icon}
    </a>
  );
};

export default NavigationIcon;
