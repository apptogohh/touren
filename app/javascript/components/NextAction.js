import React from 'react';
import Card, { CardActions, CardActionButtons } from '@material/react-card';
import Button from '@material/react-button';
import PropTypes from 'prop-types';
import MaterialIcon from '@material/react-material-icon';
import history from '../utils/history';
import { requirement, requirementSubmitted } from '../utils/locationHelpers';

const NextAction = ({ tour, locationID, locations, answers }) => {
  const idx = locations.findIndex(loc => loc.id === locationID);
  const location = locations[idx];
  const tourID = locations[idx].tourID;
  const rest = locations.slice(idx + 1);
  const nextLocation = rest.find(loc => loc.tourID === tourID);

  const nextAction =
    nextLocation &&
    (!tour.enforceSequence ||
      !requirement(location) ||
      requirementSubmitted(location, answers)) ? (
      <Button
        icon={<MaterialIcon icon="arrow_forward_ios" />}
        onClick={e => {
          e.preventDefault();
          history.push(`/tours/${tourID}/locations/${nextLocation.id}`);
        }}
      >
        {I18n.t('next_location')}
      </Button>
    ) : (
      <Button
        onClick={e => {
          e.preventDefault();
          history.push(`/tours/${tourID}`);
        }}
        icon={<MaterialIcon icon="arrow_forward_ios" />}
      >
        {I18n.t('return_to_tour')}
      </Button>
    );

  return (
    <Card outlined className="mdc-card--outlined mdc-card--padded">
      <CardActions>
        <CardActionButtons>{nextAction}</CardActionButtons>
      </CardActions>
    </Card>
  );
};

NextAction.propTypes = {
  locationID: PropTypes.number,
  locations: PropTypes.array,
  answers: PropTypes.object,
  tour: PropTypes.object
};

export default NextAction;
