import React from 'react';
import Card, { CardPrimaryContent } from '@material/react-card';
import { Body1 } from '@material/react-typography';
import Layout from '../containers/Layout';

const NotFound = () => (
  <Layout>
    <Card className="mdc-card--outlined mdc-card--padded">
      <CardPrimaryContent>
        <Body1>{I18n.t('error_not_found')}</Body1>
      </CardPrimaryContent>
    </Card>
  </Layout>
);

export default NotFound;
