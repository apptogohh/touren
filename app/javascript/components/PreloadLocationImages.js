import React from 'react';
import PropTypes from 'prop-types';

const PreloadLocationImages = ({ locations }) => {
  const imageURLs = [];
  locations.forEach(loc => {
    if (loc.thumbnailURL) {
      imageURLs.push(loc.thumbnailURL);
      loc.contentItems.forEach(item => {
        if (item.type === 'LocationImage') {
          imageURLs.push(item.imageURL);
        }
      });
    }
  });
  const images = imageURLs.map((url, id) => <img key={id} src={url} />);
  return (
    <div id="images" style={{ display: 'none' }}>
      {images}
    </div>
  );
};

PreloadLocationImages.propTypes = {
  locations: PropTypes.array
};

export default PreloadLocationImages;
