import React from 'react';
import { CardActions, CardActionButtons } from '@material/react-card';
import Button from '@material/react-button';
import PropTypes from 'prop-types';
import { Body1 } from '@material/react-typography';

const RequirementActions = ({ requirement, answer, onSubmitRequirement }) => {
  const submitted = answer && !!answer.submittedAt;
  const disabled =
    submitted ||
    (requirement.type === 'LocationMultichoice' &&
      (!answer || !answer.chosenChoices || answer.chosenChoices.length === 0));
  return (
    <CardActions>
      <CardActionButtons>
        <Button
          disabled={disabled}
          onClick={e => {
            e.preventDefault();
            onSubmitRequirement(requirement);
          }}
          outlined
        >
          {I18n.t('answer')}
        </Button>
        <Body1 className="m-0">{submitted ? I18n.t('saved') : ''}</Body1>
      </CardActionButtons>
    </CardActions>
  );
};

RequirementActions.propTypes = {
  requirement: PropTypes.object,
  answer: PropTypes.object,
  onSubmitRequirement: PropTypes.func.isRequired
};

export default RequirementActions;
