import React from 'react';
import MaterialIcon from '@material/react-material-icon';
import PropTypes from 'prop-types';

const Thumbnail = ({ alt, src, icon }) => {
  if (icon === undefined) {
    icon = 'directions';
  }
  return src ? (
    <img alt={alt} src={src} className="mdc-list-item__graphic" />
  ) : (
    <MaterialIcon icon={icon} className="mdc-list-item__graphic" />
  );
};

Thumbnail.propTypes = {
  alt: PropTypes.string,
  icon: PropTypes.string,
  src: PropTypes.string
};

export default Thumbnail;
