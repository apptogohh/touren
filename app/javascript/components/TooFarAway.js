import React from 'react';
import Card, {
  CardPrimaryContent,
  CardActions,
  CardActionButtons
} from '@material/react-card';
import { Body1 } from '@material/react-typography';
import Button from '@material/react-button';

const TooFarAway = () => (
  <Card outlined className="mdc-card--outlined mdc-card--padded">
    <CardPrimaryContent disabled={true}>
      <Body1>{I18n.t('too_far_away')}</Body1>
    </CardPrimaryContent>
    <CardActions>
      <CardActionButtons>
        <Button disabled={true}>{I18n.t('answer')}</Button>
      </CardActionButtons>
    </CardActions>
  </Card>
);

export default TooFarAway;
