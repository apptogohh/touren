import React from 'react';
import TextField, { Input } from '@material/react-text-field';
import PropTypes from 'prop-types';

const preventDefault = e => e.preventDefault();

const WrappedTextField = ({
  name,
  label,
  valueCallback,
  value,
  span = 4,
  contentType,
  textarea = false
}) => {
  const onInput = e => {
    valueCallback(e.target.value);
    preventDefault(e);
  };
  return (
    <div
      className={'mdc-layout-grid__cell mdc-layout-grid__cell--span-' + span}
    >
      <TextField label={label} textarea={textarea}>
        <Input
          onChange={preventDefault}
          onInput={onInput}
          value={value}
          name={name}
          type={contentType}
        />
      </TextField>
    </div>
  );
};

WrappedTextField.propTypes = {
  name: PropTypes.string,
  label: PropTypes.string,
  valueCallback: PropTypes.func,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  span: PropTypes.number,
  contentType: PropTypes.string,
  textarea: PropTypes.bool
};

export default WrappedTextField;
