/* eslint-disable react/prop-types */
import React from 'react';
import { connect } from 'react-redux';
import Radio, { NativeRadioControl } from '@material/react-radio';
import Button from '@material/react-button';
import { Body1 } from '@material/react-typography';
import {
  fetchGeocodings,
  setQuery,
  setLat,
  setLng,
  setLatLng,
  setName
} from '../redux/authorLocationStore';
import authenticity from '../utils/authenticity';
import WrappedTextField from '../components/WrappedTextField';

const preventDefault = e => e.preventDefault();

const AuthorLocation = ({
  geocodings,
  query,
  queryCallback,
  lat,
  latCallback,
  lng,
  lngCallback,
  name,
  nameCallback,
  latLngCallback,
  emptySearchResult,
  fetchCallback,
  fetching
}) => {
  const items = geocodings.map(gc => {
    const onChange = () => latLngCallback(gc);
    return (
      <div
        key={gc.id}
        className="mdc-layout-grid__cell mdc-layout-grid__cell--span-12 mdc-form-field"
      >
        <Radio label={gc.name}>
          <NativeRadioControl
            name="geocodings"
            value={gc.id}
            id={gc.id}
            onChange={onChange}
          />
        </Radio>
      </div>
    );
  });
  const wrongInput = (
    <Body1 className="mdc-layout-grid__cell--span-12 mdc-typography--error">
      {I18n.t('no_locations_found')}
    </Body1>
  );
  const urlParams = new URLSearchParams(window.location.search);
  const tourID = urlParams.get('tour_id');
  return (
    <form method="POST" action="/author/locations" className="mdc-layout-grid">
      <input
        type="hidden"
        name="authenticity_token"
        value={authenticity.token()}
      />
      <input type="hidden" name="location[tour_id]" value={tourID} />
      <div className="mdc-layout-grid__inner">
        <div className="mdc-layout-grid__cell mdc-layout-grid__cell--span-12">
          <h1>{I18n.t('new_location')}</h1>
          <WrappedTextField
            label={I18n.t('attributes.name')}
            name="location[name]"
            valueCallback={nameCallback}
            value={name}
            span={12}
          />
        </div>
        <div className="mdc-layout-grid__cell mdc-layout-grid__cell--span-12">
          <WrappedTextField
            label={I18n.t('attributes.address')}
            valueCallback={queryCallback}
            value={query}
          />
        </div>
        <div className="mdc-layout-grid__cell mdc-layout-grid__cell--span-12">
          <Button
            outlined
            onClick={e => {
              preventDefault(e);
              fetchCallback(query);
            }}
          >
            {I18n.t('search')}
          </Button>
        </div>
        {emptySearchResult ? (
          wrongInput
        ) : fetching ? (
          <div className="mdc-layout-grid__cell mdc-layout-grid__cell--span-12">
            <img src="/images/preloader.gif" />
          </div>
        ) : (
          items
        )}
        <WrappedTextField
          label={I18n.t('attributes.latitude')}
          name="location[lat]"
          valueCallback={latCallback}
          value={lat}
          contentType="text"
        />
        <WrappedTextField
          label={I18n.t('attributes.longitude')}
          name="location[lng]"
          valueCallback={lngCallback}
          value={lng}
          contentType="text"
        />
        <div className="mdc-layout-grid__cell mdc-layout-grid__cell--span-12">
          <Button outlined disabled={name ? false : true}>
            {I18n.t('submit')}
          </Button>
        </div>
      </div>
    </form>
  );
};

const mapStateToProps = state => {
  return {
    name: state.name,
    query: state.query,
    lat: state.lat,
    lng: state.lng,
    geocodings: state.geocodings,
    emptySearchResult: state.emptySearchResult,
    fetching: state.fetching
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchCallback: query => dispatch(fetchGeocodings(query)),
    queryCallback: query => dispatch(setQuery(query)),
    latCallback: lat => dispatch(setLat(lat)),
    lngCallback: lng => dispatch(setLng(lng)),
    latLngCallback: latLng => dispatch(setLatLng(latLng)),
    nameCallback: name => dispatch(setName(name))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AuthorLocation);
