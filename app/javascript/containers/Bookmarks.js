import React from 'react';
import List, {
  ListItem,
  ListItemGraphic,
  ListItemText,
  ListItemMeta
} from '@material/react-list';
import MaterialIcon from '@material/react-material-icon';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { toggleBookmark } from '../redux/actions';
import Thumbnail from '../components/Thumbnail';
import ItemsNotFound from '../components/ItemsNotFound';
import Layout from './Layout';

// eslint-disable-next-line react/prop-types
const Bookmarks = ({ locations, onToggleBookmark }) => {
  const listItems = locations.filter(item => item.bookmarked);

  const list = listItems.map(item => (
    <Link
      to={`/tours/${item.tourID}/locations/${item.id}`}
      key={item.id}
      style={{ textDecoration: 'none' }}
    >
      <ListItem>
        <ListItemGraphic
          graphic={
            <Thumbnail src={item.thumbnailURL} alt={item.name} icon="place" />
          }
        />
        <ListItemText primaryText={item.name} />
        <ListItemMeta
          meta={
            <MaterialIcon
              icon="bookmark"
              onClick={e => {
                e.preventDefault();
                onToggleBookmark(item.id);
              }}
            />
          }
        />
      </ListItem>
    </Link>
  ));
  return (
    <Layout title={I18n.t('bookmarks')}>
      {listItems.length > 0 ? (
        <List singleSelection>{list}</List>
      ) : (
        <ItemsNotFound>{I18n.t('missing_bookmarks')}</ItemsNotFound>
      )}
    </Layout>
  );
};

const mapStateToProps = state => {
  return {
    locations: state.locationsReducer.locations
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onToggleBookmark: id => {
      dispatch(toggleBookmark(id));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Bookmarks);
