import React from 'react';
import { connect } from 'react-redux';
import Card, { CardPrimaryContent } from '@material/react-card';
import { Body1 } from '@material/react-typography';
import { submitRequirement } from '../redux/actions';
import RequirementActions from '../components/RequirementActions';

// eslint-disable-next-line react/prop-types
const Instruction = ({ requirement, answer, onSubmitRequirement }) => {
  return (
    <Card outlined className="mdc-card--outlined mdc-card--padded">
      <CardPrimaryContent disabled={true}>
        <Body1>{requirement.instruction}</Body1>
      </CardPrimaryContent>
      <RequirementActions
        requirement={requirement}
        answer={answer}
        onSubmitRequirement={onSubmitRequirement}
      />
    </Card>
  );
};

const mapDispatchToProps = dispatch => {
  return {
    onSubmitRequirement: requirement => {
      dispatch(submitRequirement(requirement));
    }
  };
};

export default connect(
  null,
  mapDispatchToProps
)(Instruction);
