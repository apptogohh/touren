/* eslint-disable react/prop-types */
import React from 'react';
import { connect } from 'react-redux';
import Drawer, {
  DrawerAppContent,
  DrawerContent
} from '@material/react-drawer';
import List, {
  ListItem,
  ListItemGraphic,
  ListItemText
} from '@material/react-list';
import MaterialIcon from '@material/react-material-icon';
import { TopAppBarFixedAdjust } from '@material/react-top-app-bar';
import history from '../utils/history';
import HeaderBar from '../components/HeaderBar';
import NavigationIcon from '../components/NavigationIcon';
import { hideNavigation, toggleNavigation } from '../redux/actions';

const Layout = ({
  showNavigation,
  navIconClickHandler,
  children,
  navigationIcons,
  actionItems,
  title,
  navigateTo,
  isScreenSmall,
  addMargin,
  navigationCloseHandler
}) => {
  const selectedIndex =
    history.location.pathname.split('/')[1] === 'tours' ? 0 : 1;

  if (!title) {
    title = I18n.t('tours');
  }

  if (!navigationIcons) {
    if (history.location.pathname === '/tours')
      navigationIcons = (
        <NavigationIcon icon="menu" onClick={navIconClickHandler} />
      );
    else {
      navigationIcons = [
        <NavigationIcon icon="menu" key="menu" onClick={navIconClickHandler} />,
        <NavigationIcon
          icon="arrow_back_ios"
          key="arrow_back_ios"
          onClick={e => {
            e.preventDefault();
            history.goBack();
          }}
        />
      ];
    }
  }

  return (
    <div>
      <HeaderBar
        title={title}
        navigationIcons={navigationIcons}
        actionItems={actionItems}
      />

      <TopAppBarFixedAdjust>
        <Drawer
          dismissible={!isScreenSmall}
          open={showNavigation}
          modal={isScreenSmall}
          onClose={navigationCloseHandler}
        >
          <DrawerContent>
            <List singleSelection selectedIndex={selectedIndex}>
              <ListItem
                tag="a"
                onClick={() => navigateTo('tours', isScreenSmall)}
              >
                <ListItemGraphic graphic={<MaterialIcon icon="directions" />} />
                <ListItemText primaryText={I18n.t('tours')} />
              </ListItem>
              <ListItem
                tag="a"
                onClick={() => navigateTo('bookmarks', isScreenSmall)}
              >
                <ListItemGraphic graphic={<MaterialIcon icon="bookmark" />} />
                <ListItemText primaryText={I18n.t('bookmarks')} />
              </ListItem>
              <hr />
              <ListItem tag="a" href="/author/tours">
                <ListItemGraphic graphic={<MaterialIcon icon="computer" />} />
                <ListItemText primaryText={I18n.t('author_login')} />
              </ListItem>
              <ListItem tag="a" href="/imprint.html" target="_blank">
                <ListItemGraphic graphic={<MaterialIcon icon="subject" />} />
                <ListItemText primaryText={I18n.t('imprint')} />
              </ListItem>
            </List>
          </DrawerContent>
        </Drawer>
        <DrawerAppContent
          className={addMargin ? '' : 'drawer-app-content--list'}
        >
          <div className={addMargin ? 'add-margin' : ''}>{children}</div>
        </DrawerAppContent>
      </TopAppBarFixedAdjust>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    showNavigation: state.navigationReducer.showNavigation,
    isScreenSmall: state.navigationReducer.isScreenSmall
  };
};

const mapDispatchToProps = dispatch => {
  return {
    navigationCloseHandler: () => {
      dispatch(hideNavigation);
    },
    navIconClickHandler: e => {
      e.preventDefault();
      dispatch(toggleNavigation);
    },
    navigateTo: (route, isScreenSmall) => {
      if (isScreenSmall) dispatch(hideNavigation);
      history.push('/' + route);
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Layout);
