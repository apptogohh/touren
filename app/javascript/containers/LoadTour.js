/* eslint-disable react/prop-types */
import React from 'react';
import { connect } from 'react-redux';
// eslint-disable-next-line import/no-extraneous-dependencies
import { Redirect } from 'react-router';
import notFound from '../components/NotFound';
import { fetchTour, fetchTourLocations, fetchTours } from '../redux/actions';
import LoadTours from './LoadTours';
import Loading from '../components/Loading';
import LoadingError from '../components/LoadingError';

export default function LoadTour(Comp) {
  const wrapped = props => {
    const tourID = parseInt(props.match.params.tourID);

    const filteredCat = props.toursCatalog.find(item => item.id === tourID);
    if (!filteredCat) {
      return notFound();
    }

    if (
      filteredCat.passwordRequired &&
      !props.passwordUnlockedTourIds.includes(tourID)
    )
      return <Redirect to={`/tours/password?tourId=${tourID}`} />;

    if (
      props.tourLoadingErrors.includes(tourID) ||
      props.tourLocationsLoadingErrors.includes(tourID)
    ) {
      return <LoadingError />;
    }

    const currentTour = props.tours.find(item => item.id === tourID);
    if (!currentTour) {
      if (!props.tourLoading.includes(tourID)) props.fetchTour(tourID);

      return <Loading />;
    }

    if (!props.tourLocationsLoaded.includes(tourID)) {
      if (!props.tourLocationsLoading.includes(tourID)) {
        props.fetchTourLocations(tourID);
      }

      return <Loading />;
    }

    return <Comp {...props} tour={currentTour} />;
  };

  const mapStateToProps = state => {
    return {
      locations: state.locationsReducer.locations,
      tourLocationsLoading: state.locationsReducer.tourLocationsLoading,
      tourLocationsLoaded: state.locationsReducer.tourLocationsLoaded,
      tourLocationsLoadingErrors:
        state.locationsReducer.tourLocationsLoadingErrors,
      tours: state.toursReducer.tours,
      tourLoading: state.toursReducer.tourLoading,
      toursCatalog: state.toursReducer.toursCatalog,
      tourLoadingErrors: state.toursReducer.tourLoadingErrors,
      toursLoaded: state.toursReducer.toursLoaded,
      toursLoading: state.toursReducer.toursLoading,
      passwordUnlockedTourIds: state.toursReducer.passwordUnlockedTourIds
    };
  };

  const mapDispatchToProps = dispatch => {
    return {
      fetchTour: id => {
        dispatch(fetchTour(id));
      },
      fetchTourLocations: tourId => {
        dispatch(fetchTourLocations(tourId));
      },
      fetchTours: () => {
        dispatch(fetchTours());
      }
    };
  };

  return LoadTours(
    connect(
      mapStateToProps,
      mapDispatchToProps
    )(wrapped)
  );
}
