/* eslint-disable react/prop-types */
import React from 'react';
import { connect } from 'react-redux';
import { fetchTours } from '../redux/actions';
import Loading from '../components/Loading';
import LoadingError from '../components/LoadingError';

export default function LoadTours(Comp) {
  const wrapped = props => {
    if (!props.toursLoaded) {
      if (!props.toursLoading) props.fetchTours();

      return <Loading />;
    }

    if (props.toursLoadingErrors) {
      return <LoadingError />;
    }

    return <Comp {...props} />;
  };

  const mapStateToProps = state => {
    return {
      toursLoaded: state.toursReducer.toursLoaded,
      toursLoading: state.toursReducer.toursLoading,
      toursLoadingErrors: state.toursReducer.toursLoadingErrors
    };
  };

  const mapDispatchToProps = dispatch => {
    return {
      fetchTours: () => {
        dispatch(fetchTours());
      }
    };
  };

  return connect(
    mapStateToProps,
    mapDispatchToProps
  )(wrapped);
}
