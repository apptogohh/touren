/* eslint-disable react/prop-types */
import React from 'react';
import Card, { CardActions, CardActionButtons } from '@material/react-card';
import { connect } from 'react-redux';
import { toggleBookmark } from '../redux/actions';
import notFound from '../components/NotFound';
import TooFarAway from '../components/TooFarAway';
import Multichoice from './Multichoice';
import Question from './Question';
import Instruction from './Instruction';
import MapLink from '../components/MapLink';
import NavigationIcon from '../components/NavigationIcon';
import Layout from './Layout';
import LocationPanorama from '../components/LocationPanorama';
import LocationImage from '../components/LocationImage';
import LocationText from '../components/LocationText';
import LocationLink from '../components/LocationLink';
import NextAction from '../components/NextAction';
import LoadTour from './LoadTour';

const buildRequirement = (requirement, answers) => {
  if (!requirement) return undefined;

  const answer = answers[requirement.id];

  switch (requirement.type) {
    case 'LocationMultichoice':
      return (
        <Multichoice
          key={requirement.id}
          requirement={requirement}
          answer={answer}
        />
      );
    case 'LocationQuestion':
      return (
        <Question
          key={requirement.id}
          requirement={requirement}
          answer={answer}
        />
      );
    case 'LocationInstruction':
      return (
        <Instruction
          key={requirement.id}
          requirement={requirement}
          answer={answer}
        />
      );
    default:
      return undefined;
  }
};

const Location = props => {
  const locationID = parseInt(props.match.params.locationID);
  const location = props.locations.find(item => item.id === locationID);
  if (!location) {
    return notFound();
  }
  const closeBy = !props.tour.enforceProximity || location.closeBy;
  const contentItemList = location.contentItems.map(item => {
    switch (item.type) {
      case 'LocationImage':
        return <LocationImage key={item.id} item={item} />;
      case 'LocationPanorama':
        return <LocationPanorama key={item.id} item={item} />;
      case 'LocationText':
        return <LocationText key={item.id} item={item} />;
      case 'LocationLink':
        return <LocationLink key={item.id} item={item} />;
      default:
        return location.lat === null || location.lng === null || closeBy ? (
          buildRequirement(item, props.answers)
        ) : (
          <TooFarAway key={item.id} />
        );
    }
  });

  const mapLink =
    location.lat && location.lng ? (
      <Card className="mdc-card--outlined mdc-card--padded">
        <CardActions>
          <CardActionButtons>
            <MapLink location={location} />
          </CardActionButtons>
        </CardActions>
      </Card>
    ) : null;

  return (
    <Layout
      title={location.name}
      actionItems={
        <NavigationIcon
          icon={location.bookmarked ? 'bookmark' : 'bookmark_border'}
          onClick={e => {
            e.preventDefault();
            props.onToggleBookmark(locationID);
          }}
        />
      }
    >
      {mapLink}
      {contentItemList}
      <NextAction
        tour={props.tour}
        locationID={locationID}
        locations={props.locations}
        answers={props.answers}
      />
    </Layout>
  );
};

const mapStateToProps = state => {
  return {
    locations: state.locationsReducer.locations,
    tours: state.toursReducer.tours,
    answers: state.answersReducer
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onToggleBookmark: id => {
      dispatch(toggleBookmark(id));
    }
  };
};

export default LoadTour(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Location)
);
