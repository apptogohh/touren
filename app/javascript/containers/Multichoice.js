/* eslint-disable react/prop-types */
import React from 'react';
import { connect } from 'react-redux';
import Card, { CardPrimaryContent } from '@material/react-card';
import Checkbox from '@material/react-checkbox';
import { Body1 } from '@material/react-typography';
import { submitRequirement, toggleChoice } from '../redux/actions';
import RequirementActions from '../components/RequirementActions';

const Multichoice = ({
  requirement,
  answer,
  onSubmitRequirement,
  onToggleChoice
}) => {
  const choices = requirement.choices.map(choice => {
    const submitted = answer && answer.submittedAt;
    const checked = answer && answer.chosenChoices.includes(choice.choice);
    const correct = submitted && choice.correct;
    const className = correct
      ? 'mdc-form-field choice--correct'
      : 'mdc-form-field';
    return (
      <div key={choice.choice} className={className}>
        <Checkbox
          nativeControlId={choice.choice}
          disabled={submitted}
          checked={checked}
          onChange={() => onToggleChoice(requirement.id, choice.choice)}
        />
        <label htmlFor={choice.choice}>{choice.choice}</label>
      </div>
    );
  });
  return (
    <Card outlined className="mdc-card--outlined mdc-card--padded">
      <CardPrimaryContent disabled={true}>
        <Body1>{requirement.question}</Body1>
        {choices}
      </CardPrimaryContent>
      <RequirementActions
        requirement={requirement}
        answer={answer}
        onSubmitRequirement={onSubmitRequirement}
      />
    </Card>
  );
};

const mapDispatchToProps = dispatch => {
  return {
    onSubmitRequirement: requirement => {
      dispatch(submitRequirement(requirement));
    },
    onToggleChoice: (requirementID, choice) => {
      dispatch(toggleChoice(requirementID, choice));
    }
  };
};

export default connect(
  null,
  mapDispatchToProps
)(Multichoice);
