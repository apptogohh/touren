/* eslint-disable react/prop-types */
import React from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import Button from '@material/react-button';
import TextField, { Input } from '@material/react-text-field';
import { connect } from 'react-redux';
import queryString from 'query-string';
import history from '../utils/history';
import {
  submitPassword,
  passwordSet,
  setRedirected,
  fetchTour
} from '../redux/actions';
import Layout from './Layout';

// eslint-disable-next-line react/prop-types
const Password = props => {
  const preventDefault = e => e.preventDefault();
  const onInput = e => {
    props.onValueChange(e.target.value);
    preventDefault(e);
  };

  const redirectToTourId = queryString.parse(props.location.search).tourId;
  const redirectUrl = redirectToTourId
    ? `/tours/${redirectToTourId}`
    : '/tours/';

  if (props.passwordShouldRedirectToTour) {
    props.setRedirected();
    history.push(redirectUrl);
  }

  return (
    <Layout title={I18n.t('password')}>
      {redirectToTourId && <p>{I18n.t('password_required_for_this_tour')}</p>}

      <TextField label={I18n.t('attributes.password')} className="mt-4">
        <Input
          onChange={preventDefault}
          onInput={onInput}
          value={props.password}
          autoFocus
        />
      </TextField>
      <div className="mt-3">
        <Button
          raised
          onClick={e => {
            e.preventDefault();
            props.onSubmitPassword();
          }}
          className="mr-3"
        >
          {I18n.t('send')}
        </Button>
        <Button
          outlined
          onClick={e => {
            e.preventDefault();
            history.goBack();
          }}
        >
          {I18n.t('cancel')}
        </Button>
      </div>
    </Layout>
  );
};

const mapStateToProps = state => {
  return {
    tours: state.toursReducer.tours,
    password: state.toursReducer.password,
    passwordSubmitted: state.toursReducer.passwordSubmitted,
    passwordShouldRedirectToTour:
      state.toursReducer.passwordShouldRedirectToTour
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onValueChange: password => {
      dispatch(passwordSet(password));
    },
    onSubmitPassword: id => {
      dispatch(submitPassword(id));
    },
    setRedirected: () => {
      dispatch(setRedirected());
    },
    fetchTour: id => {
      dispatch(fetchTour(id));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Password);
