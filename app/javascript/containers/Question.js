/* eslint-disable react/prop-types */
import React from 'react';
import { connect } from 'react-redux';
import Card, { CardPrimaryContent } from '@material/react-card';
import { Body1 } from '@material/react-typography';
import TextField, { Input } from '@material/react-text-field';
import { submitRequirement, setAnswerText } from '../redux/actions';
import RequirementActions from '../components/RequirementActions';

const Question = ({
  requirement,
  answer,
  onValueChange,
  onSubmitRequirement
}) => {
  const preventDefault = e => e.preventDefault();

  const onInput = e => {
    onValueChange(requirement.id, e.target.value);
    preventDefault(e);
  };
  const textArea = (
    <TextField label={I18n.t('your_answer')} textarea>
      <Input
        onChange={preventDefault}
        onInput={onInput}
        value={(answer && answer.answerText) || ''}
        disabled={answer && !!answer.submittedAt}
      />
    </TextField>
  );

  return (
    <Card outlined className="mdc-card--outlined mdc-card--padded">
      <CardPrimaryContent disabled={true}>
        <Body1>{requirement.question}</Body1>
        {textArea}
      </CardPrimaryContent>
      <RequirementActions
        requirement={requirement}
        answer={answer}
        onSubmitRequirement={onSubmitRequirement}
      />
    </Card>
  );
};

const mapDispatchToProps = dispatch => {
  return {
    onValueChange: (requirementID, answer) => {
      dispatch(setAnswerText(requirementID, answer));
    },
    onSubmitRequirement: requirement => {
      dispatch(submitRequirement(requirement));
    }
  };
};

export default connect(
  null,
  mapDispatchToProps
)(Question);
