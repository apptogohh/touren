import React from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { Switch, Route, Redirect } from 'react-router';
import Tours from './Tours';
import Bookmarks from './Bookmarks';
// eslint-disable-next-line import/no-named-as-default
import Tour from './Tour';
import Location from './Location';
import Submit from './Submit';
import Password from './Password';

const Routes = () => (
  <Switch>
    <Route path="/tours/password" component={Password} />
    <Route path="/tours/:tourID/locations/:locationID" component={Location} />
    <Route path="/tours/:tourID/submit" component={Submit} />
    <Route path="/tours/:tourID" component={Tour} />
    <Route path="/tours" component={Tours} />
    <Route path="/bookmarks" component={Bookmarks} />
    <Route path="/" render={() => <Redirect to="/tours" />} />
  </Switch>
);

export default Routes;
