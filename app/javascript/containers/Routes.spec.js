import React from 'react';
import { shallow } from 'enzyme';
// eslint-disable-next-line import/no-extraneous-dependencies
import { Switch, Route } from 'react-router';
import Routes from './Routes';

describe('Routes Component Rendering', () => {
  const wrapper = shallow(<Routes />);
  test('find Switch ', () => {
    expect(wrapper.find(Switch)).toHaveLength(1);
  });
  test('find Route ', () => {
    expect(wrapper.find(Route)).toHaveLength(7);
  });
});
