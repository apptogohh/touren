/* eslint-disable react/prop-types */
import React from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { Redirect } from 'react-router';
import Button from '@material/react-button';
import TextField, { Input, HelperText } from '@material/react-text-field';
import { connect } from 'react-redux';
import history from '../utils/history';
import { submitTour, setAlias } from '../redux/actions';
import Layout from './Layout';
import LoadTour from './LoadTour';

const Submit = ({
  tour,
  match,
  alias,
  onValueChange,
  onSubmitTour,
  tourSubmitting
}) => {
  const tourID = parseInt(match.params.tourID);

  const preventDefault = e => e.preventDefault();
  const onInput = e => {
    onValueChange(e.target.value);
    preventDefault(e);
  };

  return !tour.submitted ? (
    <Layout title={I18n.t('submit_answers')}>
      <TextField
        label={I18n.t('attributes.alias')}
        helperText={
          <HelperText>
            {I18n.t('will_help_examiner_to_identify_you')}
          </HelperText>
        }
        className="mt-4"
      >
        <Input onChange={preventDefault} onInput={onInput} value={alias} />
      </TextField>
      <Button
        raised
        onClick={e => {
          e.preventDefault();
          onSubmitTour(tourID);
        }}
        className="mr-3"
        disabled={tourSubmitting.includes(tourID)}
      >
        {I18n.t('send')}
      </Button>
      <Button
        outlined
        onClick={e => {
          e.preventDefault();
          history.goBack();
        }}
      >
        {I18n.t('cancel')}
      </Button>
    </Layout>
  ) : (
    <Redirect to={`/tours/${tourID}`} />
  );
};

const mapStateToProps = state => {
  return {
    alias: state.toursReducer.alias,
    tourSubmitting: state.toursReducer.tourSubmitting
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onValueChange: alias => {
      dispatch(setAlias(alias));
    },
    onSubmitTour: id => {
      dispatch(submitTour(id));
    }
  };
};

export default LoadTour(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Submit)
);
