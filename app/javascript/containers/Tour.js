/* eslint-disable react/prop-types */
import React from 'react';
import MaterialIcon from '@material/react-material-icon';
import { connect } from 'react-redux';
import List, {
  ListItem,
  ListItemGraphic,
  ListItemText
} from '@material/react-list';
import { Link } from 'react-router-dom';
import { Body2 } from '@material/react-typography';
import Button from '@material/react-button';
import history from '../utils/history';
import NavigationIcon from '../components/NavigationIcon';
import Thumbnail from '../components/Thumbnail';
import Layout from './Layout';
import LoadTour from './LoadTour';
import PreloadLocationImages from '../components/PreloadLocationImages';
import ItemsNotFound from '../components/ItemsNotFound';
import { reloadTour } from '../redux/actions';
import {
  requirement,
  requirementSubmitted,
  requirementTypes
} from '../utils/locationHelpers';

const link = item => {
  return (
    <Link
      to={`/tours/${item.tourID}/locations/${item.id}`}
      key={item.id}
      style={{ textDecoration: 'none' }}
    >
      <ListItem>
        <ListItemGraphic
          graphic={
            <Thumbnail src={item.thumbnailURL} alt={item.name} icon="place" />
          }
        />
        <ListItemText primaryText={item.name} />
      </ListItem>
    </Link>
  );
};

const unlockFirst = item => (
  <ListItem key={item.id}>
    <ListItemGraphic graphic={<MaterialIcon icon="lock" />} />
    <ListItemText primaryText={I18n.t('unlock_first')} />
  </ListItem>
);

const answerCount = (tourID, answers) =>
  Object.values(answers).filter(answer => answer.tourID === tourID).length;

const requirementCount = (tourID, locations) =>
  locations
    .filter(loc => loc.tourID === tourID)
    .filter(loc =>
      loc.contentItems.find(item => requirementTypes.includes(item.type))
    ).length;

export const getFirstUnansweredIndex = (tour, locations, answers) => {
  if (!tour.enforceSequence) {
    return Infinity;
  }
  for (let i = 0; i < locations.length; i++) {
    if (
      requirement(locations[i]) &&
      !requirementSubmitted(locations[i], answers)
    ) {
      return i;
    }
  }
  return Infinity;
};

export const Tour = props => {
  const tourID = parseInt(props.match.params.tourID);

  const tourLocations = props.locations.filter(item => item.tourID == tourID);

  const answeredUntil = props.tour.enforceSequence
    ? getFirstUnansweredIndex(props.tour, tourLocations, props.answers)
    : Infinity;
  const locationListItems = tourLocations.map((item, i) =>
    i <= answeredUntil ? link(item) : unlockFirst(item)
  );

  const submit =
    requirementCount(tourID, tourLocations) > 0 ? (
      props.tour.submitted ? (
        <Body2>
          {I18n.t('answers_were_submitted', {
            submittedBy: props.tour.submittedBy,
            submittedAt: props.tour.submittedAt
          })}
        </Body2>
      ) : (
        <SubmitAnswers
          tourID={tourID}
          answers={props.answers}
          locations={tourLocations}
        />
      )
    ) : (
      ''
    );

  return (
    <Layout
      title={props.tour.name}
      actionItems={
        <NavigationIcon
          icon="refresh"
          onClick={e => {
            e.preventDefault();
            if (confirm(I18n.t('do_you_really_want_to_reload_tour')))
              props.reloadData(tourID);
          }}
        />
      }
    >
      {locationListItems.length > 0 ? (
        <React.Fragment>
          <List singleSelection>{locationListItems}</List>
          {submit}
          <PreloadLocationImages locations={tourLocations} />
        </React.Fragment>
      ) : (
        <ItemsNotFound>{I18n.t('missing_locations')}</ItemsNotFound>
      )}
    </Layout>
  );
};

const SubmitAnswers = ({ tourID, answers, locations }) => {
  const aCount = answerCount(tourID, answers);
  const rCount = requirementCount(tourID, locations);
  return (
    <div className="mdc-form-field">
      <Button
        onClick={e => {
          e.preventDefault();
          if (aCount >= rCount || confirm(I18n.t('submit_unfinished_answers')))
            history.push(`/tours/${tourID}/submit`);
        }}
        outlined
        style={{ fontSize: '0.6rem' }}
      >
        {I18n.t('submit_answers')}
      </Button>
      <label>
        {I18n.t('answers_to_requirements', {
          answer_count: aCount,
          requirement_count: rCount
        })}
      </label>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    locations: state.locationsReducer.locations,
    answers: state.answersReducer,
    tours: state.toursReducer.tours,
    passwordUnlockedTourIds: state.toursReducer.passwordUnlockedTourIds
  };
};

const mapDispatchToProps = dispatch => {
  return {
    reloadData: id => {
      dispatch(reloadTour(id));
    }
  };
};

export default LoadTour(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Tour)
);
