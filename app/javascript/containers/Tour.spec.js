// import React from 'react';
// import { mount } from 'enzyme';
// import { BrowserRouter as Router } from 'react-router-dom';
import { Tour, getFirstUnansweredIndex } from './Tour';
import '../utils/i18n';

// const setup = ({
//   locations = { locations: [{ id: 1, tourID: 1 }] },
//   tours = [{ id: 1, name: 'Tour 1' }],
//   match = { params: { tourID: 1 } }
// }) => {
//   const tour = Tour({
//     locations: locations,
//     tours: tours,
//     match: match,
//     t: key => key
//   });
//
//   return mount(<Router>{tour}</Router>);
// };

describe('Tour Component', () => {
  it('is defined', () => {
    expect(Tour).toBeDefined();
  });

  // it('renders info for empty locations', () => {
  //   const wrapper = setup({ locations: [] });
  //   expect(wrapper.find('.mdc-typography--body1').text()).toEqual(
  //     I18n.t('missing_locations')
  //   );
  // });
});

describe('getFirstUnansweredIndex', () => {
  it('returns Infinity if sequence not enforced', () => {
    expect(getFirstUnansweredIndex({ enforceSequence: false })).toEqual(
      Infinity
    );
  });
  it('skips locations without requirement', () => {
    expect(
      getFirstUnansweredIndex({ enforceSequence: true }, [
        { contentItems: [] },
        { contentItems: [] },
        { contentItems: [{ id: 3, type: 'LocationQuestion' }] }
      ])
    ).toEqual(2);
  });
  it('checks answers', () => {
    expect(
      getFirstUnansweredIndex(
        { enforceSequence: true },
        [
          { contentItems: [] },
          { contentItems: [] },
          { contentItems: [{ id: 3, type: 'LocationInstruction' }] },
          { contentItems: [{ id: 4, type: 'LocationMultichoice' }] }
        ],
        { 3: { submittedAt: new Date() } }
      )
    ).toEqual(3);
  });
});
