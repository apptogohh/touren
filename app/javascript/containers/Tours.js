/* eslint-disable react/prop-types */
import React from 'react';
import List, {
  ListItem,
  ListItemGraphic,
  ListItemText
} from '@material/react-list';
import Button from '@material/react-button';
import MaterialIcon from '@material/react-material-icon';
import TextField, { HelperText, Input } from '@material/react-text-field';
// import { Caption } from '@material/react-typography';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import history from '../utils/history';
import NavigationIcon from '../components/NavigationIcon';
import Layout from './Layout';
import Thumbnail from '../components/Thumbnail';
import ItemsNotFound from '../components/ItemsNotFound';
import LoadTours from './LoadTours';
import { reloadTours, searchQuerySet } from '../redux/actions';

const Tours = ({
  listItems,
  passwordUnlockedTourIds,
  toursCatalog,
  reloadData,
  searchQuery,
  search,
  searchClear
}) => {
  const filtered = listItems.filter(
    item =>
      !item.passwordRequired ||
      (passwordUnlockedTourIds && passwordUnlockedTourIds.includes(item.id))
  );

  let filteredBySearch;
  if (searchQuery) {
    filteredBySearch = filtered.filter(item =>
      item.name.toLowerCase().includes(searchQuery.toLowerCase())
    );
  } else {
    filteredBySearch = filtered;
  }

  const list = filteredBySearch.map(item => (
    <Link
      to={`/tours/${item.id}`}
      key={item.id}
      style={{ textDecoration: 'none' }}
    >
      <ListItem>
        <ListItemGraphic
          graphic={<Thumbnail alt={item.name} src={item.thumbnailURL} />}
        />
        <ListItemText
          primaryText={
            !item.passwordSubmitted
              ? item.name
              : `${item.name} (${I18n.t('unlocked')})`
          }
        />
      </ListItem>
    </Link>
  ));
  return (
    <Layout
      title={I18n.t('tours')}
      actionItems={
        <NavigationIcon
          icon="refresh"
          onClick={e => {
            e.preventDefault();
            if (confirm(I18n.t('do_you_really_want_to_reload_tours')))
              reloadData();
          }}
        />
      }
    >
      {filtered.length > 0 ? (
        <React.Fragment>
          <div className="mdc-text-field--full-width topElementMargin">
            <TextField
              label={I18n.t('filter_tours')}
              helperText={
                <HelperText>{I18n.t('input_part_of_tour_name')}</HelperText>
              }
              trailingIcon={<MaterialIcon role="button" icon="close" />}
              leadingIcon={<MaterialIcon role="button" icon="search" />}
              onTrailingIconSelect={() => searchClear()}
              outlined
            >
              <Input
                value={searchQuery}
                onChange={e => search(e.currentTarget.value)}
              />
            </TextField>
          </div>
          {searchQuery && (
            <div className="ml-3 mt-3">
              {/*<Caption>*/}
              {I18n.t('filtered_tours_count', {
                count: filteredBySearch.length
              })}
              {/*</Caption>*/}
            </div>
          )}
          {filtered.length > 0 && (
            <React.Fragment>
              <List singleSelection>{list}</List>
              {toursCatalog.filter(
                item => item.passwordRequired && !item.passwordSubmitted
              ).length > 0 && (
                <Button
                  onClick={e => {
                    e.preventDefault();
                    history.push(`/tours/password`);
                  }}
                  icon={<MaterialIcon icon="lock" />}
                  outlined
                  className="ml-3"
                >
                  {I18n.t('unlock_tours')}
                </Button>
              )}
            </React.Fragment>
          )}
        </React.Fragment>
      ) : (
        <ItemsNotFound>{I18n.t('missing_tours')}</ItemsNotFound>
      )}
    </Layout>
  );
};

const mapStateToProps = state => {
  return {
    listItems: state.toursReducer.tours,
    passwordUnlockedTourIds: state.toursReducer.passwordUnlockedTourIds,
    toursCatalog: state.toursReducer.toursCatalog,
    searchQuery: state.toursReducer.searchQuery
  };
};

const mapDispatchToProps = dispatch => {
  return {
    reloadData: () => {
      dispatch(reloadTours());
    },
    search: q => {
      dispatch(searchQuerySet(q));
    },
    searchClear: () => {
      dispatch(searchQuerySet(''));
    }
  };
};

export default LoadTours(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Tours)
);
