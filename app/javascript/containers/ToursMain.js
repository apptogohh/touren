import React from 'react';
import { Router } from 'react-router-dom';
import history from '../utils/history';
import Routes from './Routes';

const ToursMain = () => {
  return (
    <Router history={history}>
      <Routes />
    </Router>
  );
};

export default ToursMain;
