import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { store } from '../redux/authorLocationStore';
import AuthorLocation from '../containers/AuthorLocation';

// eslint-disable-next-line import/no-unresolved
import '../styles/tours';

document.addEventListener('DOMContentLoaded', () => {
  ReactDOM.render(
    <Provider store={store}>
      <AuthorLocation />
    </Provider>,
    document
      .getElementsByClassName('yieldContainer')[0]
      .appendChild(document.createElement('div'))
  );
});
