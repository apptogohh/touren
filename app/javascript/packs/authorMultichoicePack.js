import React from 'react';
import ReactDOM from 'react-dom';
import IconButton from '@material/react-icon-button';
import Button from '@material/react-button';
import Checkbox from '@material/react-checkbox';
import MaterialIcon from '@material/react-material-icon';
import List, {
  ListItem,
  ListItemMeta,
  ListItemText
} from '@material/react-list';
import PropTypes from 'prop-types';
import authenticity from '../utils/authenticity';
import WrappedTextField from '../components/WrappedTextField';

// eslint-disable-next-line import/no-unresolved
import '../styles/tours';

class AuthorMultichoice extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      choices: [],
      choiceText: '',
      choiceCorrect: false
    };
  }

  render() {
    const changeText = text => {
      this.setState({ name: text });
    };
    const changeChoiceText = text => {
      this.setState({ choiceText: text });
    };
    const changeChoiceCorrect = e => {
      this.setState({ choiceCorrect: e.target.checked });
    };
    const addChoice = e => {
      this.setState({
        choices: this.state.choices.concat([
          {
            choice: this.state.choiceText,
            correct: this.state.choiceCorrect
          }
        ]),
        choiceText: '',
        choiceCorrect: false
      });
      e.preventDefault();
    };
    const removeChoice = i => {
      const newChoices = this.state.choices.slice();
      newChoices.splice(i, 1);
      this.setState({
        choices: newChoices
      });
    };
    const locationID = window.locationID;
    const submitDisabled = !(
      !!this.state.name &&
      this.state.choices.length > 0 &&
      this.state.choiceText == ''
    );
    return (
      <form
        method="POST"
        action={`/author/locations/${locationID}/multichoices`}
        className="mdc-layout-grid"
      >
        <input
          type="hidden"
          name="authenticity_token"
          value={authenticity.token()}
        />
        <div className="mdc-layout-grid__inner">
          <div className="mdc-layout-grid__cell mdc-layout-grid__cell--span-12">
            <h1>{I18n.t('new_multichoice')}</h1>
            <WrappedTextField
              label={I18n.t('question')}
              name="location_multichoice[name]"
              valueCallback={changeText}
              value={this.state.name}
              span={12}
              textarea
            />
            <h2>{I18n.t('add_choices')}</h2>
            <List>
              {this.state.choices.map((choice, i) => {
                return (
                  <Choice
                    key={i}
                    choice={choice}
                    removeChoice={() => removeChoice(i)}
                  />
                );
              })}
            </List>
            <div className="mdc-form-field">
              <WrappedTextField
                label={I18n.t('add_choice_hint')}
                valueCallback={changeChoiceText}
                value={this.state.choiceText}
                span={12}
              />
              <div className="mdc-form-field">
                {/*
                    Adding key to checkbox to fix a bug:
                    it was not possible to create multiple correct choices
                    https://lionizers.atlassian.net/jira/software/projects/TOUR/boards/11?selectedIssue=TOUR-141
                    Unique key force react to recreate component.
                */}
                <Checkbox
                  key={new Date().getTime()}
                  nativeControlId="correctCheckbox"
                  checked={this.state.choiceCorrect}
                  onChange={changeChoiceCorrect}
                />
                <label htmlFor="correctCheckbox">
                  {I18n.t('choice_correct')}
                </label>
              </div>
              <IconButton
                disabled={this.state.choiceText === ''}
                onClick={addChoice}
              >
                <MaterialIcon icon="add_circle" />
              </IconButton>
            </div>
          </div>
          <div className="mdc-layout-grid__cell mdc-layout-grid__cell--span-12">
            <Button outlined disabled={submitDisabled}>
              {I18n.t('submit')}
            </Button>
          </div>
        </div>
      </form>
    );
  }
}

const Choice = ({ choice, removeChoice }) => {
  const clickHandler = e => {
    removeChoice();
    e.preventDefault();
  };
  return (
    <ListItem className={choice.correct ? 'choice--correct' : ''}>
      <ListItemText primaryText={choice.choice} />
      <ListItemMeta
        meta={
          <IconButton onClick={clickHandler}>
            <MaterialIcon icon="remove_circle" />
          </IconButton>
        }
      />
      <input
        name="location_multichoice[choices][]"
        type="hidden"
        value={choice.choice}
      />
      <input
        name="location_multichoice[correct_choices][]"
        type="hidden"
        value={choice.correct}
      />
    </ListItem>
  );
};

Choice.propTypes = {
  removeChoice: PropTypes.func,
  choice: PropTypes.object
};

document.addEventListener('DOMContentLoaded', () => {
  ReactDOM.render(
    <AuthorMultichoice />,
    document
      .getElementsByClassName('yieldContainer')[0]
      .appendChild(document.createElement('div'))
  );
});
