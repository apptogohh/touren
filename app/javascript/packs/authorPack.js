/* eslint-disable import/no-unresolved */

import '../utils/noty';
import 'noty/src/noty.scss';
import 'noty/src/themes/mint.scss';

import '../styles/topElementMargin';

import '../styles/author/setupMaterial';
import '../styles/author/fixMargin';
import '../styles/author/mFileUpload';
import '../styles/author/mFormField';
import '../styles/author/errorMessages';
import '../styles/author/disableSidebarBorder';
import '../styles/author/angularMaterialTables';

import '../utils/setupUJS';
import '../author/setupMaterial';
import '../author/disableEnterSubmit';
import '../author/mFileUpload';
import '../author/authorApp';
