import '../utils/noty';
import '../utils/setupUJS';
import '../utils/fetchActive';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import ToursMain from '../containers/ToursMain';
import store from '../redux/store';
import {
  clearErrors,
  checkOnline,
  watchPosition,
  watchScreenSize
} from '../redux/actions';
// eslint-disable-next-line import/no-unresolved
import '../styles/tours';

if ('serviceWorker' in navigator) {
  window.addEventListener('load', () => {
    navigator.serviceWorker.register('/sw.js');
  });
}

watchPosition(store);
store.dispatch(clearErrors());
store.dispatch(checkOnline());

window.addEventListener('resize', () => {
  store.dispatch(watchScreenSize());
});

document.addEventListener('DOMContentLoaded', () => {
  ReactDOM.render(
    <Provider store={store}>
      <ToursMain />
    </Provider>,
    document.body.appendChild(document.createElement('div'))
  );
});
