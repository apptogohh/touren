/* eslint-disable no-unused-vars */
import axios from 'axios';
import authenticity from '../utils/authenticity';
import encryptPassword from '../utils/encryptPassword';
import store from './store';
import { requirement, requirements } from '../utils/locationHelpers';
import addToCache from '../utils/addToCache';
import { BIG_SCREEN_WIDTH } from '../constants';

export const CLEAR_ERRORS = 'CLEARS_ERRORS';
export const clearErrors = () => {
  return { type: CLEAR_ERRORS };
};

export const SET_IS_SCREEN_SMALL = 'SET_IS_SCREEN_SMALL';
export const setIsScreenSmall = isScreenSmall => {
  return { type: SET_IS_SCREEN_SMALL, payload: isScreenSmall };
};

export const watchScreenSize = isScreenSmall => {
  return (dispatch, getState) => {
    const isScreenSmall = window.innerWidth < BIG_SCREEN_WIDTH;

    if (getState().navigationReducer.isScreenSmall != isScreenSmall) {
      // Doesn't work properly.
      // TODO: Make it work.
      // if (isScreenSmall) {
      //   dispatch(hideNavigation);
      // }
      // else {
      //   dispatch(showNavigation);
      // }

      dispatch(setIsScreenSmall(isScreenSmall));
    }
  };
};

export const TOGGLE_NAVIGATION = 'TOGGLE_NAVIGATION';
export const toggleNavigation = { type: TOGGLE_NAVIGATION };
export const HIDE_NAVIGATION = 'HIDE_NAVIGATION';
export const hideNavigation = { type: HIDE_NAVIGATION };
export const SHOW_NAVIGATION = 'SHOW_NAVIGATION';
export const showNavigation = { type: SHOW_NAVIGATION };

export const UPDATE_POSITION = 'UPDATE_POSITION';
export const updatePosition = position => {
  return { type: UPDATE_POSITION, payload: { position: position } };
};
export const ERROR_UPDATING_POSITION = 'ERROR_UPDATING_POSITION';
export const errorUpdatingPosition = error => {
  return { type: ERROR_UPDATING_POSITION, payload: { error: error } };
};
export const SET_ANSWER_TEXT = 'SET_ANSWER_TEXT';
export const setAnswerText = (requirementID, answerText) => {
  return {
    type: SET_ANSWER_TEXT,
    payload: { requirementID: requirementID, answerText: answerText }
  };
};
export const SET_ALIAS = 'SET_ALIAS';
export const setAlias = alias => {
  return { type: SET_ALIAS, payload: { alias: alias } };
};

export const TOGGLE_CHOICE = 'TOGGLE_CHOICE';
export const toggleChoice = (requirementID, choice) => {
  return {
    type: TOGGLE_CHOICE,
    payload: { requirementID: requirementID, choice: choice }
  };
};
export const SUBMIT_REQUIREMENT = 'SUBMIT_REQUIREMENT';
export const submitRequirement = requirement => {
  return {
    type: SUBMIT_REQUIREMENT,
    payload: { requirement: requirement }
  };
};
export const SUBMIT_TOUR_SUCCESS = 'SUBMIT_TOUR_SUCCESS';
export const submitTourSuccess = (tourId, alias, created_at_string) => {
  return {
    type: SUBMIT_TOUR_SUCCESS,
    payload: {
      tourId: tourId,
      alias: alias,
      created_at_string: created_at_string
    }
  };
};
export const SUBMIT_TOUR_ERROR = 'SUBMIT_TOUR_ERROR';
export const submitTourError = (tourId, error) => {
  return { type: SUBMIT_TOUR_ERROR, payload: { tourId: tourId, error: error } };
};
export const SUBMIT_TOUR = 'SUBMIT_TOUR';
export const submitTour = tourId => {
  return (dispatch, getState) => {
    const alias = getState().toursReducer.alias;
    if (!alias) {
      noty(I18n.t('tour_submit_alias_required'));
    } else {
      dispatch({ type: SUBMIT_TOUR, payload: { tourId: tourId } });

      const locations = getState().locationsReducer.locations;

      const tourRequirements = _.flatten(
        locations
          .filter(l => l.tourID === tourId && requirement(l))
          .map(l => requirements(l))
      );
      const tourRequirementsIds = tourRequirements.map(x => x.id);
      const answers = getState().answersReducer;
      const tourRequirementsAnswers = tourRequirementsIds.reduce(
        (obj, key) => ({ ...obj, [key]: answers[key] }),
        {}
      );

      const json = JSON.stringify({
        answers: tourRequirementsAnswers,
        requirements: tourRequirements
      });

      const password = store
        .getState()
        .toursReducer.toursCatalog.find(item => item.id === tourId).password;

      axios(`/api/v1/tours/${tourId}/answers`, {
        method: 'post',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'X-CSRF-Token': authenticity.token(),
          'X-Password': encryptPassword(password)
        },
        data: {
          answer: {
            alias: alias,
            json: json
          }
        }
      })
        .then(function(response) {
          noty(I18n.t('tour_submit_success'));
          dispatch(
            submitTourSuccess(tourId, alias, response.data.created_at_string)
          );
        })
        .catch(function(error) {
          noty(I18n.t('tour_submit_error'), 'error');
          dispatch(submitTourError(tourId, error));
        });
    }
  };
};

export const SEARCH_QUERY_SET = 'SEARCH_SET';
export const searchQuerySet = searchQuery => {
  return { type: SEARCH_QUERY_SET, payload: { searchQuery: searchQuery } };
};

export const PASSWORD_SET = 'PASSWORD_SET';
export const passwordSet = password => {
  return { type: PASSWORD_SET, payload: { password: password } };
};

export const PASSWORD_SUBMIT_SUCCESS = 'PASSWORD_SUBMIT_SUCCESS';
export const passwordSubmitSuccess = (tourId, password) => {
  return {
    type: PASSWORD_SUBMIT_SUCCESS,
    payload: { tourId: tourId, password: password }
  };
};

export const PASSWORD_SUBMIT_ERROR = 'PASSWORD_SUBMIT_ERROR';
export const passwordSubmitError = error => {
  return { type: PASSWORD_SUBMIT_ERROR, payload: { error: error } };
};

export const submitPassword = () => {
  return (dispatch, getState) => {
    const password = getState().toursReducer.password;

    axios(`/api/v1/tours/password`, {
      method: 'post',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'X-CSRF-Token': authenticity.token(),
        'X-Password': encryptPassword(password)
      }
    })
      .then(function(response) {
        dispatch(passwordSubmitSuccess(response.data.tourId, password));
        dispatch(fetchTour(response.data.tourId));
        noty(I18n.t('password_submit_success'));
      })
      .catch(function(error) {
        let m = '';
        if (
          error.response &&
          error.response.status == 400 &&
          error.response.data &&
          error.response.data.status == 'wrong_password'
        )
          m = 'password_wrong';
        else m = 'password_submit_error';

        noty(I18n.t(m), 'error');
        dispatch(passwordSubmitError(error));
      });
  };
};

export const SET_REDIRECTED = 'SET_REDIRECTED';
export const setRedirected = () => {
  return {
    type: SET_REDIRECTED
  };
};

export const TOGGLE_BOOKMARK_SUCCESS = 'TOGGLE_BOOKMARK_SUCCESS';
export const toggleBookmarkSuccess = (tourId, bookmarked) => {
  return {
    type: TOGGLE_BOOKMARK_SUCCESS,
    payload: { tourId: tourId, bookmarked: bookmarked }
  };
};

export const toggleBookmark = locationId => {
  return (dispatch, getState) => {
    const location = getState().locationsReducer.locations.find(
      item => item.id === locationId
    );
    const bookmarked = !location.bookmarked;

    if (bookmarked) noty(I18n.t('bookmark_added'));
    else noty(I18n.t('bookmark_removed'), 'error');

    dispatch(toggleBookmarkSuccess(locationId, bookmarked));
  };
};

export const fetchTours = () => dispatch => {
  if (!store.getState().toursReducer.toursLoading) {
    dispatch(toursLoading());
    return axios
      .get('/api/v1/tours')
      .then(function(response) {
        dispatch(toursLoadingSuccess(response.data));
      })
      .catch(function(error) {
        dispatch(toursLoadingError(error));
      });
  }
};

export const TOURS_LOADING = 'TOURS_LOADING';
export const toursLoading = () => {
  return { type: TOURS_LOADING };
};

export const TOURS_LOADING_SUCCESS = 'TOURS_LOADING_SUCCESS';
export const toursLoadingSuccess = data => {
  return { type: TOURS_LOADING_SUCCESS, payload: data };
};

export const TOURS_LOADING_ERROR = 'TOURS_LOADING_ERROR';
export const toursLoadingError = error => {
  return { type: TOURS_LOADING_ERROR, payload: { error: error } };
};

export const fetchTourLocations = tourId => dispatch => {
  if (
    !store.getState().locationsReducer.tourLocationsLoaded.includes(tourId) &&
    !store.getState().locationsReducer.tourLocationsLoading.includes(tourId)
  ) {
    const password = store
      .getState()
      .toursReducer.toursCatalog.find(item => item.id === tourId).password;
    const headers = password
      ? { headers: { 'X-Password': encryptPassword(password) } }
      : {};

    dispatch(tourLocationsLoading(tourId));
    return axios
      .get(`/api/v1/tours/${tourId}/locations`, headers)
      .then(function(response) {
        dispatch(tourLocationsLoadingSuccess(response.data));
      })
      .catch(function(error) {
        dispatch(tourLocationsLoadingError(error, tourId));
      });
  }
};

export const TOUR_LOCATIONS_LOADING_ERROR = 'TOUR_LOCATIONS_LOADING_ERROR';
export const tourLocationsLoadingError = (error, tourId) => {
  return {
    type: TOUR_LOCATIONS_LOADING_ERROR,
    payload: { error: error, tourId: tourId }
  };
};

export const fetchTour = id => dispatch => {
  const state = store.getState();
  if (
    !state.toursReducer.tours.find(item => item.id === id) &&
    !state.toursReducer.tourLoading[id]
  ) {
    const password = state.toursReducer.toursCatalog.find(
      item => item.id === id
    ).password;
    dispatch(tourLoading(id));

    return axios
      .get(`/api/v1/tours/${id}`, {
        headers: { 'X-Password': encryptPassword(password) }
      })
      .then(function(response) {
        dispatch(tourLoadingSuccess(response.data));
        dispatch(fetchTourLocations(id));
      })
      .catch(function(error) {
        dispatch(tourLoadingError(error, id));
      });
  }
};

export const RELOAD_TOUR = 'RELOAD_TOUR';
export const reloadTour = id => {
  noty(I18n.t('tour_data_was_cleared'));
  return { type: RELOAD_TOUR, payload: { id: id } };
};

export const RELOAD_TOURS = 'RELOAD_TOURS';
export const reloadTours = () => {
  noty(I18n.t('tours_data_was_cleared'));
  return { type: RELOAD_TOURS };
};

export const TOUR_LOADING = 'TOUR_LOADING';
export const tourLoading = tourId => {
  return { type: TOUR_LOADING, payload: { tourId: tourId } };
};

export const TOUR_LOADING_SUCCESS = 'TOUR_LOADING_SUCCESS';
export const tourLoadingSuccess = data => {
  return { type: TOUR_LOADING_SUCCESS, payload: data };
};

export const TOUR_LOADING_ERROR = 'TOURS_LOADING_ERROR';
export const tourLoadingError = (error, id) => {
  return { type: TOUR_LOADING_ERROR, payload: { error: error, id: id } };
};

export const TOUR_LOCATIONS_LOADING = 'TOUR_LOCATIONS_LOADING';
export const tourLocationsLoading = tourId => {
  return { type: TOUR_LOCATIONS_LOADING, payload: { tourId: tourId } };
};

export const TOUR_LOCATIONS_LOADING_SUCCESS = 'TOUR_LOCATIONS_LOADING_SUCCESS';
export const tourLocationsLoadingSuccess = data => {
  return { type: TOUR_LOCATIONS_LOADING_SUCCESS, payload: data };
};

export const checkOnline = () => dispatch =>
  axios
    .get('/favicon.ico?_=' + new Date().getTime())
    .then(function(responce) {
      dispatch(setIsOnline(true));

      return dispatch(fetchTours());

      // noty('online');
    })
    .catch(function(error) {
      dispatch(setIsOnline(false));
      // noty('offline', 'error');
    });

export const SET_IS_ONLINE = 'SET_IS_ONLINE';
export const setIsOnline = isOnline => {
  return { type: SET_IS_ONLINE, payload: isOnline };
};

export const watchPosition = store => {
  if ('geolocation' in navigator) {
    navigator.geolocation.watchPosition(
      position => store.dispatch(updatePosition(posToLatLng(position))),
      error => store.dispatch(errorUpdatingPosition(error))
    );
  } else {
    store.dispatch(
      errorUpdatingPosition('geolocation not accessible (not in navigator)')
    );
  }
};
const posToLatLng = position => {
  return { lat: position.coords.latitude, lng: position.coords.longitude };
};
