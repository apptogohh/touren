import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import axios from 'axios';
import updateObject from '../utils/updateObject';
import authenticity from '../utils/authenticity';

const defaultState = {
  geocodings: [],
  name: '',
  query: '',
  lat: '',
  lng: '',
  emptySearchResult: false,
  fetching: false
};

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore((state = defaultState, action) => {
  switch (action.type) {
    case 'SET_QUERY':
      return updateObject(state, { query: action.payload.query });
    case 'SET_LAT':
      return updateObject(state, { lat: action.payload.lat });
    case 'SET_LNG':
      return updateObject(state, { lng: action.payload.lng });
    case 'SET_LATLNG':
      return updateObject(state, {
        lat: action.payload.lat,
        lng: action.payload.lng
      });
    case 'SET_NAME':
      return updateObject(state, { name: action.payload.name });
    case 'GEOCODING_START':
      return updateObject(state, { fetching: true });
    case 'GEOCODING_SUCCESS':
      if (action.payload.geocodings.length === 0) {
        return updateObject(state, {
          fetching: false,
          emptySearchResult: true,
          geocodings: []
        });
      } else {
        return updateObject(state, {
          fetching: false,
          emptySearchResult: false,
          geocodings: action.payload.geocodings
        });
      }
    case 'GEOCODING_ERROR':
      return updateObject(state, {
        fetching: false,
        emptySearchResult: false,
        geocodings: []
      });
    default:
      return state;
  }
}, composeEnhancers(applyMiddleware(thunk)));

export const fetchGeocodings = q => {
  return dispatch => {
    dispatch({ type: 'GEOCODING_START' });
    axios('/api/v1/geocodings', {
      credentials: 'same-origin',
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'X-CSRF-Token': authenticity.token()
      },
      data: { q: q }
    })
      .then(function(response) {
        dispatch({ type: 'GEOCODING_SUCCESS', payload: response.data });
      })
      .catch(function(error) {
        // eslint-disable-next-line no-console
        console.log(error);

        if (error.response.status == 429)
          noty(I18n.t('error_too_many_search_requests'), 'error');

        dispatch({ type: 'GEOCODING_ERROR', payload: error });
      });
  };
};

export const geocodingStart = result => {
  return { type: 'GEOCODING_START', payload: result };
};

export const geocodingSuccess = result => {
  return { type: 'GEOCODING_SUCCESS', payload: result };
};

export const geocodingError = result => {
  return { type: 'GEOCODING_ERROR', payload: result };
};

export const setQuery = query => {
  return { type: 'SET_QUERY', payload: { query: query } };
};

export const setLat = lat => {
  return { type: 'SET_LAT', payload: { lat: lat } };
};

export const setLng = lng => {
  return { type: 'SET_LNG', payload: { lng: lng } };
};

export const setLatLng = latLng => {
  return { type: 'SET_LATLNG', payload: { lat: latLng.lat, lng: latLng.lng } };
};

export const setName = name => {
  return { type: 'SET_NAME', payload: { name: name } };
};
