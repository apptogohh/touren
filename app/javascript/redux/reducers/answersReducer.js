import {
  SET_ANSWER_TEXT,
  TOGGLE_CHOICE,
  SUBMIT_REQUIREMENT,
  RELOAD_TOUR,
  RELOAD_TOURS
} from '../actions';
import updateObject from '../../utils/updateObject';

const answersReducer = (state = defaultState, action) => {
  switch (action.type) {
    case TOGGLE_CHOICE:
      return toggleChoice(
        state,
        action.payload.requirementID,
        action.payload.choice
      );
    case SET_ANSWER_TEXT:
      return setAnswerText(
        state,
        action.payload.requirementID,
        action.payload.answerText
      );
    case SUBMIT_REQUIREMENT:
      return submitRequirement(state, action.payload.requirement);
    case RELOAD_TOUR:
      return reloadTour(state, action.payload.id);
    case RELOAD_TOURS:
      return reloadTours();
    default:
      return state;
  }
};

const defaultState = {};

const setAnswerText = (state, requirementID, answerText) => {
  const answer = state[requirementID];
  if (answer && answer.submittedAt) {
    return state;
  }
  return updateObject(state, { [requirementID]: { answerText: answerText } });
};

const toggleChoice = (state, requirementID, choice) => {
  const answer = state[requirementID];
  if (answer && answer.submittedAt) {
    return state;
  }
  if (!answer) {
    return updateObject(state, {
      [requirementID]: { chosenChoices: [choice] }
    });
  }
  const choiceIncluded = answer.chosenChoices.includes(choice);
  const newChoices = choiceIncluded
    ? answer.chosenChoices.filter(ch => ch !== choice)
    : answer.chosenChoices.concat(choice);

  return updateObject(state, {
    [requirementID]: updateObject(answer, { chosenChoices: newChoices })
  });
};

const submitRequirement = (state, requirement) => {
  const answer = state[requirement.id];
  if (answer && answer.submittedAt) {
    return state;
  }
  const data = {
    tourID: requirement.tourID,
    submittedAt: new Date().getTime(),
    type: requirement.type
  };
  if (answer) {
    return updateObject(state, {
      [requirement.id]: updateObject(answer, data)
    });
  }
  return updateObject(state, { [requirement.id]: data });
};

const reloadTour = (state, id) => {
  return _.pickBy(state, item => item.tourID != id);
};

const reloadTours = () => {
  return defaultState;
};

export default answersReducer;
