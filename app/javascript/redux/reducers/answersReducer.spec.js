import answersReducer from './answersReducer';
import { toggleChoice } from '../actions';

describe('Toggling choice', () => {
  it('adds choice when none exists', () => {
    const state = { '1': { chosenChoices: ['1997', '1999'] } };
    const actual = answersReducer(state, toggleChoice(1, '1998'))['1'];
    expect(actual.chosenChoices).toEqual(['1997', '1999', '1998']);
  });
  it('removes choice when exists', () => {
    const state = { '1': { chosenChoices: ['1997', '1998', '1999'] } };
    const actual = answersReducer(state, toggleChoice(1, '1998'))['1'];
    expect(actual.chosenChoices).toEqual(['1997', '1999']);
  });
  it('does not add choice when already submitted', () => {
    const state = {
      '1': { chosenChoices: ['1997', '1999'], submittedAt: 1234 }
    };
    const actual = answersReducer(state, toggleChoice(1, '1998'))['1'];
    expect(actual.chosenChoices).toEqual(['1997', '1999']);
  });
});
