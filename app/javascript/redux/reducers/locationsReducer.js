/* eslint-disable no-case-declarations */
import {
  CLEAR_ERRORS,
  UPDATE_POSITION,
  ERROR_UPDATING_POSITION,
  TOGGLE_BOOKMARK_SUCCESS,
  TOUR_LOCATIONS_LOADING,
  TOUR_LOCATIONS_LOADING_SUCCESS,
  TOUR_LOCATIONS_LOADING_ERROR,
  RELOAD_TOUR,
  RELOAD_TOURS
} from '../actions';
import updateObject from '../../utils/updateObject';

const CLOSE_BY_DISTANCE_METERS = 20;

// Adjust for dev/testing purposes. Should match seeds.rb.
const fallbackPosition = { lat: 53.4543505, lng: 9.9628131 };

const emptyState = {
  locations: [],
  locationsLoading: false,
  locationsLoaded: false,
  locationsLoadingErrors: false,
  tourLocationsLoading: [],
  tourLocationsLoaded: [],
  tourLocationsLoadingErrors: [],
  position: fallbackPosition
};

const locationsReducer = (state = emptyState, action) => {
  switch (action.type) {
    case CLEAR_ERRORS:
      return updateObject(state, {
        locationsLoading: false,
        locationsLoadingErrors: false,
        tourLocationsLoading: [],
        tourLocationsLoadingErrors: []
      });
    case RELOAD_TOUR:
      return updateObject(state, {
        locations: state.locations.filter(item => item.id != action.payload.id),
        tourLocationsLoading: state.tourLocationsLoading.filter(
          item => item != action.payload.id
        ),
        tourLocationsLoaded: state.tourLocationsLoaded.filter(
          item => item != action.payload.id
        ),
        tourLocationsLoadingErrors: state.tourLocationsLoadingErrors.filter(
          item => item != action.payload.id
        )
      });
    case RELOAD_TOURS:
      return emptyState;
    case TOUR_LOCATIONS_LOADING:
      return updateObject(state, {
        tourLocationsLoading: _.uniq([
          ...state.tourLocationsLoading,
          action.payload.tourId
        ])
      });
    case TOUR_LOCATIONS_LOADING_SUCCESS:
      const newLocations = state.locations.filter(
        loc => loc.tourID != action.payload.tourId
      );

      return updateObject(state, {
        locations: newLocations.concat(action.payload.locations),
        tourLocationsLoading: state.tourLocationsLoading.filter(
          item => item != action.payload.tourId
        ),
        tourLocationsLoaded: _.uniq([
          ...state.tourLocationsLoaded,
          action.payload.tourId
        ]),
        tourLocationsLoadingErrors: state.tourLocationsLoadingErrors.filter(
          item => item != action.payload.tourId
        )
      });
    case UPDATE_POSITION: {
      const ids = closeByIDs(state.locations, action.payload.position);
      const locs = updateCloseBys(state.locations, ids);
      return updateObject(state, {
        locations: locs,
        position: action.payload.position
      });
    }
    case ERROR_UPDATING_POSITION: {
      const ids = closeByIDs(state.locations, state.position);
      const locs = updateCloseBys(state.locations, ids);
      return updateObject(state, { locations: locs });
    }
    case TOUR_LOCATIONS_LOADING_ERROR:
      // eslint-disable-next-line no-console
      console.log(action.payload.error);
      return updateObject(state, {
        tourLocationsLoadingErrors: _.uniq([
          ...state.tourLocationsLoadingErrors,
          action.payload.tourId
        ]),
        tourLocationsLoading: state.tourLocationsLoading.filter(
          item => item != action.payload.tourId
        )
      });
    case TOGGLE_BOOKMARK_SUCCESS:
      const index = state.locations.findIndex(
        item => item.id == action.payload.tourId
      );

      const toggledLocation = updateObject(state.locations[index], {
        bookmarked: action.payload.bookmarked
      });

      return updateObject(state, {
        locations: Object.assign([...state.locations], {
          [index]: toggledLocation
        })
      });
    default:
      return state;
  }
};

export default locationsReducer;

const updateCloseBys = (locations, closeByLocationIDs) =>
  locations.map(location =>
    updateObject(location, {
      closeBy: closeByLocationIDs.includes(location.id)
    })
  );

// https://stackoverflow.com/a/27943/261006
const distance = (latLng1, latLng2) => {
  const R = 6371; // Radius of the earth in km
  const dLat = deg2rad(latLng2.lat - latLng1.lat); // deg2rad below
  const dLon = deg2rad(latLng2.lng - latLng1.lng);
  const a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(deg2rad(latLng1.lat)) *
      Math.cos(deg2rad(latLng2.lat)) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2);
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  const d = R * c; // Distance in km
  return d * 1000;
};

const deg2rad = deg => deg * (Math.PI / 180);

const closeByIDs = (locations, position) =>
  locations
    .filter(
      loc =>
        loc.lat && loc.lng && distance(loc, position) < CLOSE_BY_DISTANCE_METERS
    )
    .map(location => location.id);
