import {
  HIDE_NAVIGATION,
  SHOW_NAVIGATION,
  TOGGLE_NAVIGATION,
  SET_IS_SCREEN_SMALL
} from '../actions';
import updateObject from '../../utils/updateObject';
import { BIG_SCREEN_WIDTH } from '../../constants';

let showNavigation, isScreenSmall;
if (window.innerWidth >= BIG_SCREEN_WIDTH) {
  isScreenSmall = false;
  showNavigation = true;
} else {
  isScreenSmall = true;
  showNavigation = false;
}

const defaultState = {
  showNavigation,
  isScreenSmall
};

const navigationReducer = (state = defaultState, action) => {
  switch (action.type) {
    case TOGGLE_NAVIGATION:
      return updateObject(state, { showNavigation: !state.showNavigation });
    case HIDE_NAVIGATION:
      return updateObject(state, { showNavigation: false });
    case SHOW_NAVIGATION:
      return updateObject(state, { showNavigation: true });
    case SET_IS_SCREEN_SMALL:
      return updateObject(state, { isScreenSmall: action.payload });
    default:
      return state;
  }
};

export default navigationReducer;
