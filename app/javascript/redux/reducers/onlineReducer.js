import { SET_IS_ONLINE } from '../actions';
import updateObject from '../../utils/updateObject';

const emptyState = {
  isOnline: false
};

const onlineReducer = (state = emptyState, action) => {
  switch (action.type) {
    case SET_IS_ONLINE:
      return updateObject(state, {
        isOnline: action.payload
      });
    default:
      return state;
  }
};

export default onlineReducer;
