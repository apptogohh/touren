/* eslint-disable no-case-declarations */
import {
  CLEAR_ERRORS,
  TOURS_LOADING_ERROR,
  SET_ALIAS,
  PASSWORD_SET,
  PASSWORD_SUBMIT_SUCCESS,
  SET_REDIRECTED,
  TOURS_LOADING,
  TOURS_LOADING_SUCCESS,
  TOUR_LOADING,
  TOUR_LOADING_SUCCESS,
  TOUR_LOADING_ERROR,
  RELOAD_TOUR,
  RELOAD_TOURS,
  SEARCH_QUERY_SET,
  SUBMIT_TOUR,
  SUBMIT_TOUR_SUCCESS,
  SUBMIT_TOUR_ERROR
} from '../actions';
import updateObject from '../../utils/updateObject';

const emptyState = {
  toursCatalog: [],
  toursLoading: false,
  toursLoaded: false,
  toursLoadingErrors: false,
  tourLoading: [],
  tourLoadingErrors: [],
  tourSubmitting: [],
  tours: [],
  alias: '',
  password: '',
  passwordUnlockedTourIds: [],
  passwordShouldRedirectToTour: false,
  searchQuery: ''
};

const toursReducer = (state = emptyState, action) => {
  switch (action.type) {
    case CLEAR_ERRORS:
      return updateObject(state, {
        toursLoading: false,
        toursLoadingErrors: false,
        tourLoading: [],
        tourLoadingErrors: []
      });
    case RELOAD_TOUR:
      return updateObject(state, {
        tours: state.tours.filter(item => item.id != action.payload.id),
        tourLoading: state.tourLoading.filter(
          item => item != action.payload.id
        ),
        tourLoadingErrors: state.tourLoadingErrors.filter(
          item => item != action.payload.id
        ),
        toursLoaded: false,
        toursLoadingErrors: false
      });
    case RELOAD_TOURS:
      return emptyState;
    case TOUR_LOADING:
      return updateObject(state, {
        tourLoading: _.uniq([...state.tourLoading, action.payload.tourId])
      });
    case TOUR_LOADING_SUCCESS:
      let toursAltered = updateObject(state.tours);
      if (!state.tours.find(item => item.id == action.payload.id))
        toursAltered = [...state.tours, action.payload.tour];

      return updateObject(state, {
        tourLoading: state.tourLoading.filter(
          item => item != action.payload.tour.id
        ),
        tourLoadingErrors: state.tourLoadingErrors.filter(
          item => item != action.payload.tour.id
        ),
        tours: toursAltered
      });
    case TOURS_LOADING:
      return updateObject(state, {
        toursLoading: true
      });
    case TOURS_LOADING_SUCCESS:
      let toursToAdd = [];
      action.payload.tours.forEach(function(item) {
        if (!state.tours.find(tour => tour.id == item.id)) {
          toursToAdd.push(item);
        }
      });

      let toursCatalogToAdd = [];
      action.payload.toursCatalog.forEach(function(item) {
        if (!state.toursCatalog.find(cat => cat.id == item.id)) {
          toursCatalogToAdd.push(item);
        }
      });

      return updateObject(state, {
        toursCatalog: [...state.toursCatalog, ...toursCatalogToAdd],
        tours: [...state.tours, ...toursToAdd],
        toursLoading: false,
        toursLoaded: true,
        toursLoadingErrors: false
      });
    case TOURS_LOADING_ERROR:
      // eslint-disable-next-line no-console
      console.log(action.payload.error);
      return updateObject(state, {
        toursLoading: false,
        toursLoadingErrors: true
      });
    case TOUR_LOADING_ERROR:
      // eslint-disable-next-line no-console
      console.log(action.payload.error);
      return updateObject(state, {
        tourLoading: state.tourLoading.filter(
          item => item != action.payload.tour.id
        ),
        tourLoadingErrors: _.uniq([
          ...state.tourLoadingErrors,
          action.payload.tourId
        ])
      });
    case SET_ALIAS:
      return updateObject(state, { alias: action.payload.alias });
    case SUBMIT_TOUR:
      return updateObject(state, {
        tourSubmitting: [...state.tourSubmitting, action.payload.tourId]
      });
    case SUBMIT_TOUR_SUCCESS:
      const index = state.tours.findIndex(
        item => item.id == action.payload.tourId
      );

      const submittedTour = updateObject(state.tours[index], {
        submitted: true,
        submittedAt: action.payload.created_at_string,
        submittedBy: action.payload.alias
      });

      return updateObject(state, {
        tours: Object.assign([...state.tours], { [index]: submittedTour }),
        tourSubmitting: state.tourSubmitting.filter(
          item => item != action.payload.tourId
        )
      });
    case SUBMIT_TOUR_ERROR:
      return updateObject(state, {
        tourSubmitting: state.tourSubmitting.filter(
          item => item != action.payload.tourId
        )
      });
    case SEARCH_QUERY_SET:
      return updateObject(state, { searchQuery: action.payload.searchQuery });
    case PASSWORD_SET:
      return updateObject(state, { password: action.payload.password });
    case PASSWORD_SUBMIT_SUCCESS:
      const tourCatalogIndex = state.toursCatalog.findIndex(
        item => item.id == action.payload.tourId
      );

      const alteredTourCatalog = updateObject(
        state.toursCatalog[tourCatalogIndex],
        {
          passwordSubmitted: true,
          password: action.payload.password
        }
      );

      let alteredPasswordUnlockedTourIds = [...state.passwordUnlockedTourIds];
      alteredPasswordUnlockedTourIds.push(action.payload.tourId);

      return updateObject(state, {
        password: '',
        passwordSubmitted: true,
        passwordUnlockedTourIds: alteredPasswordUnlockedTourIds,
        passwordShouldRedirectToTour: true,
        toursCatalog: Object.assign([...state.toursCatalog], {
          [tourCatalogIndex]: alteredTourCatalog
        })
      });
    case SET_REDIRECTED:
      return updateObject(state, {
        passwordShouldRedirectToTour: false
      });
    default:
      return state;
  }
};

export default toursReducer;
