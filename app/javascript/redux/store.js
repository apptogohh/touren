import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import throttle from 'lodash/throttle';
import navigationReducer from './reducers/navigationReducer';
import toursReducer from './reducers/toursReducer';
import locationsReducer from './reducers/locationsReducer';
import answersReducer from './reducers/answersReducer';
import onlineReducer from './reducers/onlineReducer';
import { loadState, saveState } from './localStorage';
// import {createLogger} from 'redux-logger';

// const logger = createLogger({});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  combineReducers({
    navigationReducer,
    toursReducer,
    locationsReducer,
    answersReducer,
    onlineReducer
  }),
  loadState(),
  composeEnhancers(applyMiddleware(thunk))
);

// To be able to access store in browser console.
window.store = store;

store.subscribe(
  throttle(() => {
    saveState(store.getState());
  })
);

export default store;
