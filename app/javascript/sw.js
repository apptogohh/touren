/* eslint-disable no-undef */
const cacheName = 'touren-cache';

workbox.core.skipWaiting();
workbox.core.clientsClaim();

const spaPrecache = {
  revision: '5', // this has to be manually increased for each release
  url: '/tours'
};

const precacheManifest = self.__precacheManifest
  ? self.__precacheManifest.concat(spaPrecache)
  : [spaPrecache];

workbox.precaching.precacheAndRoute(precacheManifest);

workbox.routing.registerRoute(
  '/tours',
  new workbox.strategies.NetworkFirst({ cacheName: cacheName })
);

try {
  workbox.routing.registerNavigationRoute(
    workbox.precaching.getCacheKeyForURL('/tours'),
    {
      whitelist: [new RegExp('^/tours/')]
    }
  );
} catch (error) {
  // eslint-disable-next-line no-console
  console.warn(error);
}

workbox.routing.registerRoute(
  new RegExp('/rails/'),
  new workbox.strategies.CacheFirst({
    cacheName: cacheName
  })
);

workbox.routing.registerRoute(
  new RegExp('/javascripts/'),
  new workbox.strategies.NetworkFirst({ cacheName: cacheName })
);

workbox.routing.registerRoute(
  new RegExp('/material-icons/'),
  new workbox.strategies.NetworkFirst({ cacheName: cacheName })
);

workbox.routing.registerRoute(
  new RegExp('/packs/'),
  new workbox.strategies.NetworkFirst({ cacheName: cacheName })
);

workbox.routing.registerRoute(
  new RegExp('/images/'),
  new workbox.strategies.NetworkFirst({ cacheName: cacheName })
);
