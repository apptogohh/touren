import 'babel-polyfill';

export default async urls => {
  // eslint-disable-next-line no-console
  console.log('caching', urls);
  const tourenCache = await window.caches.open('touren-cache');
  await tourenCache.addAll(urls);
};
