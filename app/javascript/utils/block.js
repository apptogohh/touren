const Block = function(blockName) {
  // CSS class for blockName
  this.b = `.${blockName}`;

  // Generate CSS selector for blockName and find it using jQuery
  this.bf = function() {
    return $(this.b);
  };

  // Generate CSS selector for elementName
  this.e = function(elementName) {
    return `.${blockName}__${elementName}`;
  };

  // Generate CSS selector for elementName and find it using jQuery
  this.ef = function(elementName) {
    return $(this.e(elementName));
  };
};

const block = function(blockName) {
  return new Block(blockName);
};

export default block;

// BEM for CSS modules
const BlockM = function(moduleName, blockName) {
  const generateScopedName = function(moduleName, localName) {
    const encode = name =>
      Buffer.from(name)
        .toString('base64')
        .replace(/\W/g, '');
    const selector = moduleName + '__' + localName;
    return 'touren__' + selector + '__' + encode(selector);
  };

  // CSS class for blockName
  this.b = `.${generateScopedName(moduleName, blockName)}`;

  // Generate CSS selector for blockName and find it using jQuery
  this.bf = function() {
    return $(this.b);
  };

  // Generate CSS selector for elementName
  this.e = function(elementName) {
    return `.${generateScopedName(moduleName, `${blockName}__${elementName}`)}`;
  };

  // Generate CSS selector for elementName and find it using jQuery
  this.ef = function(elementName) {
    return $(this.e(elementName));
  };
};

const blockm = function(moduleName, blockName = '') {
  if (!blockName) blockName = moduleName;

  return new BlockM(moduleName, blockName);
};

export { blockm };
