import { sha256 } from 'js-sha256';

export default password => sha256(password + 'GZTQdg2besnSqsKB');
