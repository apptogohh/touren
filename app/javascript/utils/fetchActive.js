// https://github.com/github/fetch/issues/516

(function(window, fetch) {
  window.fetchActive = 0;
  const inc = () => window.fetchActive++;
  const dec = () => window.fetchActive--;

  return function() {
    const req = fetch.apply(undefined, [].slice.apply(arguments, 0));
    inc();
    req.then(dec, dec);
    return req;
  };
})(window, fetch);
