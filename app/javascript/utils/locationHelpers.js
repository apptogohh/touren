export const requirementTypes = [
  'LocationInstruction',
  'LocationQuestion',
  'LocationMultichoice'
];

export const isRequirement = contentItem =>
  requirementTypes.includes(contentItem.type);

export const requirement = location =>
  location.contentItems.filter(item => isRequirement(item))[0];

export const requirements = location =>
  location.contentItems.filter(item => isRequirement(item));

export const requirementSubmitted = (location, answers) => {
  const req = requirement(location);
  return req && answers && answers[req.id] && answers[req.id].submittedAt;
};
