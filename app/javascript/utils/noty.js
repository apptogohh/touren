import Noty from 'noty';

window.noty = function(text, type = 'information') {
  return new Noty({
    text: text,
    type: type,
    dismissQueue: true,
    layout: 'topCenter',
    closeWith: ['click', 'button'],
    timeout: 5000
  }).show();
};
