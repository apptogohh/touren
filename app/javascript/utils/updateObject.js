// https://redux.js.org/recipes/structuring-reducers/refactoring-reducer-example
export default (oldObject, newValues) =>
  Object.assign({}, oldObject, newValues);
