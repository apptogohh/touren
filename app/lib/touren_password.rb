class TourenPassword
  def self.encrypt(password)
    Digest::SHA256.hexdigest(password + 'GZTQdg2besnSqsKB')
  end
end
