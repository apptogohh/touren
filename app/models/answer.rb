# == Schema Information
#
# Table name: answers
#
#  id         :bigint(8)        not null, primary key
#  alias      :string
#  json       :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  tour_id    :bigint(8)        not null
#
# Indexes
#
#  index_answers_on_tour_id  (tour_id)
#
# Foreign Keys
#
#  fk_rails_...  (tour_id => tours.id)
#

class Answer < ApplicationRecord
  belongs_to :tour
  has_one :author, through: :tour, autosave: false

  validates :alias, presence: true
  validates :tour, presence: true
  validates :json, presence: true
end
