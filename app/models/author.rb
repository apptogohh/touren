# == Schema Information
#
# Table name: authors
#
#  id                 :bigint(8)        not null, primary key
#  current_sign_in_at :datetime
#  current_sign_in_ip :string
#  email              :string           not null
#  last_sign_in_at    :datetime
#  last_sign_in_ip    :string
#  sign_in_count      :integer          default(0), not null
#  uid                :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_authors_on_email  (email) UNIQUE
#

class Author < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable, :omniauthable
  # :database_authenticatable, :registerable, :recoverable, :rememberable
  # and :validatable
  devise :saml_authenticatable, :trackable

  has_many :tours, dependent: :destroy
  has_many :answers, through: :tours, dependent: :destroy
  has_many :locations, through: :tours, dependent: :destroy
  has_many :location_content_items, through: :locations, dependent: :destroy
end
