class Geocoding
  attr_accessor :name, :lat, :lng

  def self.search(searchterm)
    return [] if searchterm.blank?

    Geocoder.search(searchterm, language: :de).map do |result|
      g = Geocoding.new
      g.name = result.display_name
      g.lat = result.latitude
      g.lng = result.longitude
      g
    end
  end

  def as_api_json
    {
      id: "#{lat}#{lng}",
      name: name,
      lat: lat,
      lng: lng
    }
  end
end
