# frozen_string_literal: true

# == Schema Information
#
# Table name: locations
#
#  id         :bigint(8)        not null, primary key
#  lat        :float
#  lng        :float
#  name       :string           not null
#  position   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  tour_id    :bigint(8)        not null
#
# Indexes
#
#  index_locations_on_tour_id  (tour_id)
#
# Foreign Keys
#
#  fk_rails_...  (tour_id => tours.id)
#

class Location < ApplicationRecord
  include Rails.application.routes.url_helpers

  belongs_to :tour
  acts_as_list scope: :tour

  has_one :author, through: :tour, autosave: false

  has_many :location_content_items, -> { order(position: :asc) }, dependent: :destroy
  has_many :location_images, dependent: :destroy
  has_many :location_panoramas, dependent: :destroy
  has_many :location_texts, dependent: :destroy
  has_many :location_multichoices, dependent: :destroy
  has_many :location_questions, dependent: :destroy
  has_many :location_instructions, dependent: :destroy
  has_many :location_links, dependent: :destroy

  scope :without_password, -> { joins(:tour).where(tours: { password_hash: [nil, ''] }) }

  def available_content_items
    return LocationContentItem::CONTENT_ITEM_TYPES unless requirement

    LocationContentItem::CONTENT_ITEM_TYPES_WITHOUT_REQUIREMENTS
  end

  def requirement
    location_content_items.where(type: LocationContentItem::REQUIREMENT_TYPES).first
  end

  def thumbnail_url # rubocop:disable Metrics/MethodLength
    items = location_content_items.to_a
    main_image = items.find { |item| item.type == LocationImage.to_s }

    if main_image.present? && main_image.location_image_file.attached? # rubocop:disable Style/GuardClause
      rails_representation_path(
        main_image.location_image_file.variant(
          combine_options: {
            auto_orient: true,
            gravity: 'center',
            resize: '200x200^',
            crop: '200x200+0+0'
          }
        ).processed,
        only_path: true
      )
    end
  end

  def as_api_json # rubocop:disable Metrics/MethodLength
    items = location_content_items.to_a

    h = {
      id: id,
      tourID: tour_id,
      createdAt: created_at,
      name: name,
      lat: lat,
      lng: lng,
      closeBy: false,
      visited: false,
      bookmarked: false,
      thumbnailURL: thumbnail_url,
      contentItems: items.map(&:as_api_json)
    }
    h
  end
end
