# == Schema Information
#
# Table name: location_content_items
#
#  id          :bigint(8)        not null, primary key
#  content     :json
#  description :string
#  name        :string
#  position    :integer
#  type        :string           not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  location_id :bigint(8)        not null
#
# Indexes
#
#  index_location_content_items_on_location_id  (location_id)
#
# Foreign Keys
#
#  fk_rails_...  (location_id => locations.id)
#

class LocationContentItem < ApplicationRecord
  belongs_to :location
  acts_as_list scope: :location

  has_one :tour, through: :location, autosave: false
  has_one :author, through: :author, autosave: false

  has_one_attached :location_image_file, dependent: :destroy
  validates :name, presence: true

  REQUIREMENT_TYPES = %w[LocationMultichoice LocationQuestion LocationInstruction].freeze
  CONTENT_ITEM_TYPES_WITHOUT_REQUIREMENTS = %w[LocationImage LocationText LocationPanorama LocationLink].freeze
  CONTENT_ITEM_TYPES = (CONTENT_ITEM_TYPES_WITHOUT_REQUIREMENTS + REQUIREMENT_TYPES).freeze

  scope :without_password, -> { joins(location: :tour).where(tours: { password_hash: [nil, ''] }) }
  scope :requirements, -> { where(type: REQUIREMENT_TYPES) }

  def as_api_json
    {
      id: id,
      type: type.to_s,
      name: name,
      locationID: location_id,
      tourID: location.tour.id
    }
  end
end
