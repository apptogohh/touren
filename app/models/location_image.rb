# == Schema Information
#
# Table name: location_content_items
#
#  id          :bigint(8)        not null, primary key
#  content     :json
#  description :string
#  name        :string
#  position    :integer
#  type        :string           not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  location_id :bigint(8)        not null
#
# Indexes
#
#  index_location_content_items_on_location_id  (location_id)
#
# Foreign Keys
#
#  fk_rails_...  (location_id => locations.id)
#

class LocationImage < LocationContentItem
  include Rails.application.routes.url_helpers

  validates :description, presence: true
  validates :location_image_file, attached: true

  def image_url
    if location_image_file.attached? # rubocop:disable Style/GuardClause
      rails_representation_path(
        location_image_file.variant(
          resize: '720x720'
        ).processed,
        only_path: true
      )
    end
  end

  def as_api_json
    super.merge(
      imageURL: image_url,
      description: description
    )
  end
end
