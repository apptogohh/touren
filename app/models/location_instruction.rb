# == Schema Information
#
# Table name: location_content_items
#
#  id          :bigint(8)        not null, primary key
#  content     :json
#  description :string
#  name        :string
#  position    :integer
#  type        :string           not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  location_id :bigint(8)        not null
#
# Indexes
#
#  index_location_content_items_on_location_id  (location_id)
#
# Foreign Keys
#
#  fk_rails_...  (location_id => locations.id)
#

class LocationInstruction < LocationContentItem
  def as_api_json
    super.merge(
      instruction: content
    )
  end
end
