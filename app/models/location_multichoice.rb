# == Schema Information
#
# Table name: location_content_items
#
#  id          :bigint(8)        not null, primary key
#  content     :json
#  description :string
#  name        :string
#  position    :integer
#  type        :string           not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  location_id :bigint(8)        not null
#
# Indexes
#
#  index_location_content_items_on_location_id  (location_id)
#
# Foreign Keys
#
#  fk_rails_...  (location_id => locations.id)
#

class LocationMultichoice < LocationContentItem
  validate :choices_format

  def choices
    return [] unless content.present?

    JSON.parse(content)['choices'].map do |attrs|
      c = Choice.new
      c.attributes = attrs
      c
    end
  end

  def question
    JSON.parse(content)['question']
  end

  def as_api_json
    super.merge(JSON.parse(content).symbolize_keys)
  end

  private

  class Choice
    include ActiveModel::Serializers::JSON
    include ActiveModel::Validations

    attr_accessor :choice, :correct
    validate :choice_format

    def as_api_json
      attributes
    end

    def attributes=(hash)
      hash.each do |key, value|
        send("#{key}=", value)
      end
    end

    def attributes
      instance_values
    end

    private

    def choice_format
      errors.add(:content, :malformed_choice) if !choice.is_a?(String) || choice.empty?
    end
  end

  def choices_format
    if choices.empty?
      errors.add(:content, :no_choices)
    elsif !choices.inject(true) { |valid, choice| valid && choice.valid? }
      errors.add(:content, :choice_without_text)
    end
  rescue StandardError
    errors.add(:content, :malformed_json)
  end
end
