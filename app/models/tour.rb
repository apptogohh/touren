# == Schema Information
#
# Table name: tours
#
#  id                :bigint(8)        not null, primary key
#  enforce_proximity :boolean
#  enforce_sequence  :boolean
#  name              :string           not null
#  password_hash     :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  author_id         :bigint(8)        not null
#
# Indexes
#
#  index_tours_on_author_id  (author_id)
#
# Foreign Keys
#
#  fk_rails_...  (author_id => authors.id)
#

class Tour < ApplicationRecord
  include Rails.application.routes.url_helpers

  attr_accessor :password

  before_validation :set_password_hash

  belongs_to :author
  has_many :locations, -> { order(position: :asc) }, dependent: :destroy
  has_many :answers, dependent: :destroy
  has_one_attached :tour_image_file, dependent: :destroy

  validates :author, presence: true
  validates :name, presence: true
  # validates :tour_image_file, attached: true
  validates :password_hash, uniqueness: { allow_blank: true }, presence: true, if: -> { password.present? }

  scope :without_password, -> { where(tours: { password_hash: [nil, ''] }) }

  def thumbnail_url # rubocop:disable Metrics/MethodLength
    if tour_image_file.attached? # rubocop:disable Style/GuardClause
      rails_representation_path(
        tour_image_file.variant(
          combine_options: {
            auto_orient: true,
            gravity: 'center',
            resize: '200x200^',
            crop: '200x200+0+0'
          }
        ).processed,
        only_path: true
      )
    end
  end

  def as_api_json # rubocop:disable Metrics/MethodLength
    {
      id: id,
      name: name,
      enforceSequence: enforce_sequence.present?,
      enforceProximity: enforce_proximity.present?,
      submitted: false,
      submittedAt: nil,
      submittedBy: nil,
      passwordSubmitted: false,
      passwordRequired: password_hash.present?,
      thumbnailURL: thumbnail_url
    }
  end

  # For catalog of all tours we just need ID and info whether it is password protected.
  # We do not want to publish sensitive data of protected tours before password was given.
  def as_api_json_catalog
    {
      id: id,
      passwordRequired: password_hash.present?,
      passwordSubmitted: false,
      password: ''
    }
  end

  def set_password_hash
    self.password_hash = TourenPassword.encrypt(password) if password.present?
  end
end
