class Rack::Attack
  Rack::Attack.cache.store = ActiveSupport::Cache::MemoryStore.new 

  throttle('limit geocodings', limit: 1, period: 10) do |req|
    if req.path == '/api/v1/geocodings' && req.post?
      req.ip
    end
  end
end
