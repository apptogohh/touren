Rails.application.routes.draw do
  devise_for :authors
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get '/', to: redirect('/tours')
  get '/bookmarks', to: 'tours#index'
  get '/tours(/*path)', to: 'tours#index', as: 'tours'

  namespace :author do
    resources :tours
    resources :locations

    resources :locations do
      get 'move_higher', on: :member
      resources :location_images, path: 'images'
      resources :location_panoramas, path: 'panoramas'
      resources :location_texts, path: 'texts'
      resources :location_multichoices, path: 'multichoices'
      resources :location_questions, path: 'questions'
      resources :location_instructions, path: 'instructions'
      resources :location_links, path: 'links'
      resources :location_content_items, path: 'content_items' do
        get 'move_higher', on: :member
      end
    end

    resources :answers
  end

  namespace :api do
    namespace :v1 do
      resources :tours do
        collection do
          post 'password' => 'tours#password'
        end

        resources :answers
        resources :locations
      end
      resources :geocodings
    end
  end
end
