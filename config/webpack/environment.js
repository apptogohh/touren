const { environment } = require('@rails/webpacker');
// eslint-disable-next-line import/no-extraneous-dependencies
const webpack = require('webpack');
const WorkboxPlugin = require('workbox-webpack-plugin');

environment.plugins.append(
  'workbox',
  new WorkboxPlugin.InjectManifest({
    swSrc: './app/javascript/sw.js',
    swDest: '../sw.js',
    globDirectory: './public',
    globPatterns: [
      'imprint.html',
      'images/preloader.gif',
      'material-icons/icons.css',
      'material-icons/icons.woff2',
      '/javascripts/i18n.js',
      '/javascripts/translations.js'
    ]
  })
);

environment.plugins.prepend(
  'Provide',
  new webpack.ProvidePlugin({
    $: 'jquery',
    jQuery: 'jquery',
    jquery: 'jquery',
    _: 'lodash'
  })
);

const merge = require('webpack-merge');

['css', 'sass'].forEach(function(item) {
  const generateScopedName = (localName, resourcePath) => {
    const base64Encode = name =>
      Buffer.from(name)
        .toString('base64')
        .replace(/\W/g, '');

    const fileName = resourcePath.split('/').pop();
    const componentName = fileName.replace(`.module.${item}`, '');
    const isRailsModule = resourcePath.indexOf('/styles/author/') != -1;
    const selector = componentName + '__' + localName;

    const out = isRailsModule
      ? 'touren__' + selector + '__' + base64Encode(selector)
      : 'touren__' + selector + '__' + base64Encode(resourcePath);

    return out;
  };

  const myCssLoaderOptions = {
    sourceMap: true,
    // eslint-disable-next-line no-unused-vars
    getLocalIdent: (context, localIdentName, localName, options) => {
      return generateScopedName(localName, context.resourcePath);
    }
  };

  const CSSLoader = environment.loaders
    .get(`module${item.charAt(0).toUpperCase() + item.slice(1)}`)
    .use.find(el => el.loader === 'css-loader');

  CSSLoader.options = merge(CSSLoader.options, myCssLoaderOptions);
});

module.exports = environment;
