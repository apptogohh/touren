class CreateTours < ActiveRecord::Migration[5.2]
  def change
    create_table :tours do |t|
      t.string :name, null: false
      t.string :password
      t.references :author, foreign_key: true, null: false

      t.timestamps
    end
  end
end
