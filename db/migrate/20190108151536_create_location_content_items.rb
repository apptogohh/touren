class CreateLocationContentItems < ActiveRecord::Migration[5.2]
  def change
    create_table :location_content_items do |t|
      t.string :type, null: false
      t.references :location, foreign_key: true, null: false
      t.references :author, foreign_key: true, null: false
      t.string :name
      t.string :description
      t.json :content

      t.timestamps
    end
    # add_foreign_key :location_content_items, :locations
  end
end
