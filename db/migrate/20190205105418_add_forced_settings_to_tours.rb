class AddForcedSettingsToTours < ActiveRecord::Migration[5.2]
  def change
    add_column :tours, :enforce_sequence, :boolean
    add_column :tours, :enforce_proximity, :boolean
  end
end
