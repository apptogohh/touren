class CreateAnswers < ActiveRecord::Migration[5.2]
  def change
    create_table :answers do |t|
      t.references :author, foreign_key: true, null: false
      t.references :tour, foreign_key: true, null: false
      t.integer :requirement_id
      t.text :json

      t.timestamps
    end
  end
end
