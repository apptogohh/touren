class ChangeAnswers < ActiveRecord::Migration[5.2]
  def change
    remove_column :answers, :requirement_id
    add_column :answers, :alias, :string
  end

  def down
    add_column :answers, :requirement_id, :integer
    remove_column :answers, :alias
  end
end
