class AddPasswordHashToTours < ActiveRecord::Migration[5.2]
  def change
    add_column :tours, :password_hash, :string
  end
end
