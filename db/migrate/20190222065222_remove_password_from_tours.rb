class RemovePasswordFromTours < ActiveRecord::Migration[5.2]
  def up
    remove_column :tours, :password
  end

  def down
    add_column :tours, :password, :string
  end
end
