class MakeLocationLatLngOptional < ActiveRecord::Migration[5.2]
  def change
    change_column :locations, :lat, :float, null: true
    change_column :locations, :lng, :float, null: true
  end
end
