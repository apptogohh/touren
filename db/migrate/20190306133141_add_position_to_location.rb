class AddPositionToLocation < ActiveRecord::Migration[5.2]
  def change
    add_column :locations, :position, :integer
  end
end
