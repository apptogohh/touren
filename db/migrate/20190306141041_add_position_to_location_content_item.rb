class AddPositionToLocationContentItem < ActiveRecord::Migration[5.2]
  def change
    add_column :location_content_items, :position, :integer
  end
end
