class RemoveRedundantAuthorId < ActiveRecord::Migration[5.2]
  def change
    # If there is some data in db table, this migration will be irreversible:
    # it would be impossible set `null: false` without assigning value to this column

    remove_reference :answers, :author, foreign_key: true, null: false
    remove_reference :locations, :author, foreign_key: true, null: false
    remove_reference :location_content_items, :author, foreign_key: true, null: false
  end
end
