config = YAML.load_file 'config/webpacker.yml'

namespace :webpacker do
  desc 'Precompile assets locally and then rsync to web servers'
  task :precompile do
    run_locally do
      with rails_env: :production do
        execute :rails, 'webpacker:clobber'
        execute :rails, 'webpacker:compile'
      end
    end

    on roles(:web), in: :parallel do |server|
      run_locally do
        execute :rsync,
                "-a ./public/#{config['production']['source_entry_path']}/ nutzer@#{server.hostname}:#{shared_path}/public/#{config['production']['source_entry_path']}/" # rubocop:disable Metrics/LineLength
        execute :rsync,
                "-a ./public/sw.js nutzer@#{server.hostname}:#{shared_path}/public/sw.js"
      end
    end

    run_locally do
      execute :rm, "-rf public/#{config['production']['source_entry_path']}"
      execute :rm, 'public/sw.js'
    end
  end
end
