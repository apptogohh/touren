def alias_task(name, old_name)
  t = Rake::Task[old_name]
  desc t.full_comment if t.full_comment
  task name, *t.arg_names do |_, args|
    # values_at is broken on Rake::TaskArguments
    all_arguments = t.arg_names.map { |a| args[a] }
    all_arguments += args.extras.to_a
    t.invoke(*all_arguments)
  end
end
