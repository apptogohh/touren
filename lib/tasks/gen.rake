# rubocop:disable Style/SymbolArray, Style/ParallelAssignment
namespace :gen do # rubocop:disable Metrics/BlockLength
  # Usage examples:
  # rake gen:create[factory]
  # rake gen:create[factory,trait]
  task :create, [:p1, :p2] => :environment do |_, args|
    if args[:p2].present?
      name, trait = args[:p1].underscore.singularize.to_sym, args[:p2].to_sym
      FactoryBot.create name, trait
    else
      name = args[:p1].underscore.to_sym
      FactoryBot.create name
    end
  end

  alias_task :c, :create

  # Usage examples:
  # rake gen:create_list[factory,10]
  # rake gen:create_list[factory,10,trait]
  task :create_list, [:p1, :p2, :p3] => :environment do |_, args|
    if args[:p3].present?
      name, num, trait = args[:p1].underscore.singularize.to_sym, args[:p2].to_i, args[:p3].to_sym
      FactoryBot.create_list name, num, trait
    else
      name, num = args[:p1].underscore.singularize.to_sym, args[:p2].to_i
      FactoryBot.create_list name, num
    end
  end

  alias_task :cl, :create_list

  # Usage examples:
  # rake gen:destroy[Model]
  # rake gen:destroy[Model1,Model2,Model3]
  # rake gen:destroy[model1,model2,model3]
  task destroy: :environment do |_, args|
    args.extras.each do |param|
      param.camelize.singularize.classify.constantize.destroy_all
    end
  end

  alias_task :d, :destroy

  # Truncate all tables
  task truncate: :environment do
    # For PostgreSQL
    conn = ActiveRecord::Base.connection
    tables = conn.execute("
      SELECT table_name
      FROM information_schema.tables
      WHERE table_type = 'BASE TABLE' AND table_schema NOT IN ('pg_catalog', 'information_schema');
    ").map { |r| r['table_name'] }
    tables.delete 'schema_migrations'
    tables.delete 'ar_internal_metadata'
    tables.each { |table| conn.execute("TRUNCATE TABLE #{table} CASCADE") }

    # # For SQLite
    # conn = ActiveRecord::Base.connection
    # tables = conn.execute("SELECT * FROM sqlite_master WHERE type='table';").map { |item| item['name'] }
    # tables.delete 'sqlite_sequence'
    # tables.delete 'schema_migrations'
    # tables.delete 'ar_internal_metadata'
    # conn.execute('PRAGMA foreign_keys=OFF')
    # tables.each { |table| conn.execute("delete from #{table}") }
    # conn.execute('PRAGMA foreign_keys=ON')
  end

  alias_task :t, :truncate
end
# rubocop:enable Style/SymbolArray, Style/ParallelAssignment
