# == Schema Information
#
# Table name: answers
#
#  id         :bigint(8)        not null, primary key
#  alias      :string
#  json       :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  tour_id    :bigint(8)        not null
#
# Indexes
#
#  index_answers_on_tour_id  (tour_id)
#
# Foreign Keys
#
#  fk_rails_...  (tour_id => tours.id)
#

FactoryBot.define do
  factory :answer do
    after(:build)  do |answer|
      answer.alias = Faker::Creature::Cat.name

      answer.tour = create(:tour, :with_locations) if answer.tour.blank?

      json_hash = {}
      json_hash['answers'] = {}
      json_hash['requirements'] = []

      answer.tour.locations.each do |l|
        l.location_content_items.requirements.each do |lci|
          answer_hash = {}
          answer_hash['tourID'] = l.tour.id
          answer_hash['submittedAt'] = Faker::Time.between(2.days.ago, Date.today).to_i

          if lci.type == 'LocationMultichoice'
            answer_hash['chosenChoices'] = JSON.parse(lci.content)['choices']
                                               .select { |item| item['correct'] }
                                               .map { |item| item['choice'] }
          elsif lci.type == 'LocationQuestion'
            answer_hash['answerText'] = Faker::ChuckNorris.fact
          end

          json_hash['answers'][lci.id] = answer_hash
        end
        json_hash['requirements'] += l.location_content_items.requirements.map(&:as_api_json)
      end
      answer.json = json_hash.to_json
    end
  end
end
