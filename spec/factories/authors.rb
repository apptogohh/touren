# == Schema Information
#
# Table name: authors
#
#  id                 :bigint(8)        not null, primary key
#  current_sign_in_at :datetime
#  current_sign_in_ip :string
#  email              :string           not null
#  last_sign_in_at    :datetime
#  last_sign_in_ip    :string
#  sign_in_count      :integer          default(0), not null
#  uid                :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_authors_on_email  (email) UNIQUE
#

FactoryBot.define do
  factory :author do
    email { "author.#{SecureRandom.hex}@example.com" }
  end
end
