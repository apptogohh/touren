FactoryBot.define do
  factory :location do
    tour
    name { Faker::Address.city }

    # lat { Faker::Address.latitude }
    # lng { Faker::Address.longitude }

    lat { 53.4543505 }
    lng { 9.9628131 }

    trait :with_content do
      after :create do |location|
        create :location_multichoice, location: location
        create :location_question, location: location
        create :location_instruction, location: location
        create :location_text, location: location
        create :location_image, location: location
        create :location_link, location: location
        create :location_panorama, location: location
      end
    end
  end
end
