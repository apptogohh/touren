FactoryBot.define do
  factory :location_image do
    location
    name { Faker::Lorem.question }
    description { Faker::ChuckNorris.fact }

    location_image_file do
      Rack::Test::UploadedFile.new(
        Rails.root.join('spec/upload_samples/porsche.jpg'),
        'application/jpeg',
        true
      )
    end
  end
end
