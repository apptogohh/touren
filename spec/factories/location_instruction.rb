FactoryBot.define do
  factory :location_instruction do
    location
    name { "Instruction: #{Faker::BossaNova.song}" }
    description { Faker::ChuckNorris.fact }
    content do
      # Faker::BossaNova.song
      # Faker::Hacker.verb
      Faker::Hipster.sentence
    end
  end
end
