FactoryBot.define do
  factory :location_link do
    location
    name { "Link: #{Faker::BossaNova.song}" }
    description { Faker::ChuckNorris.fact }
    content { Faker::Internet.url }
  end
end
