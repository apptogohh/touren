FactoryBot.define do
  factory :location_multichoice do
    location
    sequence :name do |n|
      "Multiple Choice #{n}"
    end
    description { Faker::ChuckNorris.fact }

    # First and last choices are correct
    content do
      r = rand(5..10)
      json = {
        question: Faker::Lorem.question,
        choices: Array.new(r) do |i|
                   {
                     choice: Faker::Vehicle.unique.make_and_model,
                     correct: i == 0 || i == r - 1 # rubocop:disable Style/MultipleComparison
                   }
                 end
      }.to_json
      Faker::Vehicle.unique.clear
      json
    end
  end
end
