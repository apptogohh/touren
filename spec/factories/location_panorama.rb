FactoryBot.define do
  factory :location_panorama do
    location
    name { "Panorama: #{Faker::Address.community}" }
    description { Faker::ChuckNorris.fact }
    location_image_file do
      Rack::Test::UploadedFile.new(
        Rails.root.join('spec/upload_samples/porsche.jpg'),
        'application/jpeg',
        true
      )
    end
  end
end
