FactoryBot.define do
  factory :location_question do
    location
    name { "Question: #{Faker::Ancient.god}" }
    description { Faker::ChuckNorris.fact }
    content { Faker::Lorem.question }
  end
end
