FactoryBot.define do
  factory :location_text do
    location
    name { Faker::Books::Lovecraft.tome }
    content { Faker::Books::Lovecraft.paragraphs.join("\r\n") }
  end
end
