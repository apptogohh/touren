FactoryBot.define do
  factory :tour do
    author
    name { Faker::TvShows::BreakingBad.character }
    tour_image_file do
      Rack::Test::UploadedFile.new(
        Rails.root.join('spec/upload_samples/porsche.jpg'),
        'application/jpeg',
        true
      )
    end

    trait :with_locations do
      after :create do |tour|
        create_list :location, 3, :with_content, tour: tour
        create_list :answer, 3, tour: tour
      end
    end

    trait :with_password do
      password { Time.now.strftime('%M') }
    end
  end
end
