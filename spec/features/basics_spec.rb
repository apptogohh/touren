require 'rails_helper'

RSpec.describe 'Basic Specs', type: :feature, js: true do
  before { I18n.locale = :de }

  scenario 'missing tours' do
    visit '/tours'
    expect(page).to have_content(I18n.t('missing_tours'))
  end

  context 'submit tour' do
    let!(:tour) { create(:tour, :with_locations) }

    scenario 'there should be tours' do
      visit '/tours'

      wait_for_ajax

      expect(page).to have_content(I18n.t('tours'))
      expect(page).not_to have_content(I18n.t('missing_tours'))
      expect(page).to have_content(tour.name)

      visit "/tours/#{tour.id}"

      wait_for_ajax

      expect(page).to have_content(tour.locations.first.name)

      visit "/tours/#{tour.id}/locations/#{tour.locations.first.id}"

      expect(page).to have_content(tour.locations.first.name)
      expect(page).to have_content(tour.locations.first.location_multichoices.first.choices.first.choice)

      first(:css, '.mdc-form-field label').click

      all('.mdc-button')[1].click # Submit multichoice

      fill_in(I18n.t('your_answer'), with: 'foobar')

      all('.mdc-button')[2].click # Submit question answer

      all('.mdc-button')[3].click # Submit instruction

      expect(page).to have_content(I18n.t('saved'), count: 3)

      visit "/tours/#{tour.id}"

      click_button(I18n.t('submit_answers'))

      fill_in(I18n.t('attributes.alias'), with: 'foobar')

      click_button(I18n.t('send'))

      expect(page).to have_content(I18n.t('tour_submit_success'))
    end
  end
end
