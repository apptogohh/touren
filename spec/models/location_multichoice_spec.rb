# == Schema Information
#
# Table name: location_content_items
#
#  id          :bigint(8)        not null, primary key
#  content     :json
#  description :string
#  name        :string
#  position    :integer
#  type        :string           not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  location_id :bigint(8)        not null
#
# Indexes
#
#  index_location_content_items_on_location_id  (location_id)
#
# Foreign Keys
#
#  fk_rails_...  (location_id => locations.id)
#

require 'rails_helper'

RSpec.describe LocationMultichoice, type: :model do
  context 'valid multichoice' do
    it 'validates' do
      multichoice = build(:location_multichoice)
      expect(multichoice).to be_valid
    end

    it '#as_api_json' do
      multichoice = create(
        :location_multichoice,
        id: 1,
        type: 'multichoice',
        name: 'Multiple Choice',
        content: {
          question: 'When moon?',
          choices: [{ choice: '2019', correct: true }]
        }.to_json
      )
      expect(multichoice.as_api_json).to eq(
        id: 1,
        tourID: multichoice.tour.id,
        type: 'multichoice',
        name: 'Multiple Choice',
        locationID: multichoice.location_id,
        question: 'When moon?',
        choices: [{ 'choice' => '2019', 'correct' => true }]
      )
    end
  end
  context 'missing content' do
    it 'does not validate' do
      multichoice = build(:location_multichoice, content: '')
      expect(multichoice).not_to be_valid
    end
  end
  context 'missing choices' do
    it 'does not validate' do
      multichoice = build(:location_multichoice, content: '[]')
      expect(multichoice).not_to be_valid
    end
  end
  context 'empty choice' do
    it 'does not validate' do
      multichoice = build(:location_multichoice, content: '[{"choice":"","valid":true}]')
      expect(multichoice).not_to be_valid
    end
  end
end
