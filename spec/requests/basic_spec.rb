# rubocop:disable Metrics/LineLength
require 'rails_helper'

RSpec.describe 'Basic Specs', type: :request do
  context 'all urls' do
    it 'return success' do
      include Rack::Test::Methods
      include ActionDispatch::TestProcess

      upload_sample = Rails.root.join('spec/upload_samples/porsche.jpg')

      get '/'
      expect(response).to have_http_status(301)

      get_urls = [
        '/manifest.json',
        '/bookmarks',
        '/tours',
        '/api/v1/tours'
      ]

      author = FactoryBot.create(:author)
      sign_in author
      get_urls.each do |u|
        get u
        expect(response).to have_http_status(200)
      end

      post_url = '/api/v1/geocodings'
      post_data = { q: 'qux' }
      post post_url, params: post_data
      expect(response).to have_http_status(200)

      get_url = '/author/tours/new'
      get get_url
      expect(response).to have_http_status(200)

      post_url = '/author/tours'
      tour = FactoryBot.create(:tour, author: author)
      post_data = tour.as_json
      post_data[:tour_image_file] = Rack::Test::UploadedFile.new(upload_sample, 'application/jpeg', true)
      post post_url, params: { tour: post_data }
      expect(response).to have_http_status(302)

      get_url = "/author/tours/#{Tour.last.id}"
      get get_url
      expect(response).to have_http_status(200)

      delete_url = "/author/tours/#{Tour.last.id}"
      delete delete_url
      expect(response).to have_http_status(302)

      location = FactoryBot.create(:location, author: author, tour: tour)

      get_url = "/author/locations/#{location.id}/texts/new"
      get get_url
      expect(response).to have_http_status(200)

      post_url = "/author/locations/#{location.id}/texts"
      post_data = FactoryBot.build(:location_text, tour: tour).as_api_json
      post post_url, params: { location_text: post_data }
      expect(response).to have_http_status(302)

      # get_url = "/author/locations/#{LocationText.last.location.id}/texts/#{LocationText.last.id}"
      # get get_url
      # expect(response).to have_http_status(200)

      delete_url = "/author/locations/#{LocationText.last.location.id}/texts/#{LocationText.last.id}"
      delete delete_url
      expect(response).to have_http_status(302)

      get_url = "/author/locations/#{location.id}/images/new"
      get get_url
      expect(response).to have_http_status(200)

      post_url = "/author/locations/#{location.id}/images"
      post_data = FactoryBot.build(:location_image).as_json
      post_data[:location_image_file] = Rack::Test::UploadedFile.new(upload_sample, 'application/jpeg', true)
      post post_url, params: { location_image: post_data }
      expect(response).to have_http_status(302)

      # get_url = "/author/locations/#{LocationImage.last.location.id}/texts/#{LocationImage.last.id}"
      # get get_url
      # expect(response).to have_http_status(200)

      delete_url = "/author/locations/#{LocationImage.last.location.id}/images/#{LocationImage.last.id}"
      delete delete_url
      expect(response).to have_http_status(302)

      get_url = "/author/locations/#{location.id}/multichoices/new"
      get get_url
      expect(response).to have_http_status(200)

      post_url = "/author/locations/#{location.id}/multichoices"
      post_data = FactoryBot.build(:location_multichoice).as_json
      post post_url, params: { location_multichoice: post_data }
      expect(response).to have_http_status(302)

      # get_url = "/author/locations/#{LocationMultichoice.last.location.id}/multichoices/#{LocationMultichoice.last.id}"
      # get get_url
      # expect(response).to have_http_status(200)

      delete_url = "/author/locations/#{LocationMultichoice.last.location.id}/multichoices/#{LocationMultichoice.last.id}"
      delete delete_url
      expect(response).to have_http_status(302)
    end
  end
end
# rubocop:enable Metrics/LineLength
